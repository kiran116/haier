<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\CartController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SpecialofferController;
use App\Http\Controllers\MyorderController;
use App\Http\Controllers\MywishlistController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('login',[AccountController::class,'loginview'])->name('login');
// Route::post('login',[AccountController::class,'logincheck'])->name('logincheck');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('homepage');
Route::get('/home',[AccountController::class,'home'])->name('home');

//static pages routes
Route::get('/',[HomeController::class,'index'])->name('home');
Route::get('about-us',[HomeController::class,'about'])->name('about-us');
Route::get('contact-us',[HomeController::class,'contact'])->name('contact-us');
Route::get('terms-and-condition',[HomeController::class,'termsandcondition'])->name('terms-and-condition');
Route::get('privacy-policy',[HomeController::class,'privacypolicy'])->name('privacy-policy');
Route::get('special-offer',[SpecialofferController::class,'specialoffer'])->name('special-offer');
// Route::get('header',[HeaderController::class,'header'])->name('header');
    
Route::get('my-order',[AccountController::class,'myorder'])->name('my-order');
Route::get('shop',[ShopController::class,'shop'])->name('shop');
Route::get('my-wishlist',[AccountController::class,'wishlist'])->name('my-wishlist');
Route::get('personal-details',[AccountController::class,'personaldetails'])->name('personal-details');
Route::get('change-password',[AccountController::class,'changepassword'])->name('change-password');
// Route::get('subscription-details',[MywishlistController::class,'subscriptiondetails'])->name('subscription-details');
Route::get('subscription-details',[AccountController::class,'subscriptiondetails'])->name('subscription-details');
Route::get('shopping-cart',[CartController::class,'index'])->name('shopping-cart');

Route::get('products/{c_pt_name}/{c_disp_name}/{id}/',[ProductController::class,'product_details'])->name('product_details');
Route::get('products/pdf',[ProductController::class,'loadpdf'])->name('product-info');
Route::get('shop/{id}',[ShopController::class,'shop'])->name('shop');
// Route::get('product/{id}',[ProductController::class,'product_details'])->name('product');

Route::post('addtocart',[CartController::class,'addToCart'])->name('addtocart');
Route::get('testcart',[CartController::class,'index']);
//routes for dynamic data
// Route::get('category')
