
$(document).ready(function () {
    $(".home-slider-wrap .owl-carousel").owlCarousel({
        //loop: false,
        margin: 0,
        items: 1,
        smartSpeed: 1500,
        autoplay: false,
        navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],

    });

    // $(".flagship-carousal.owl-carousel").owlCarousel({
    //     loop: !1,
    //     margin: 0,
    //     nav: !0,
    //     dots: !1,
    //     navText: ["<i class='fa fa-chevron-left'></i>"],
    // });
    $(".flagship-carousal.owl-carousel").owlCarousel({ 
        // loop: !1,
        margin: 0, 
        nav: true, 
        dots: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: { 0: { items: 1 }, 480: { items: 1 }, 678: { items: 3 }, 960: { items: 3 }, 1160: { items: 3 } } 
    });

    $(".category-wrapper").owlCarousel({
        loop:false,
        margin:0,
        nav:true,
        dots:false,
        navText:["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        responsive:{0:{items:1},480:{items:1},678:{items:3},960:{items:3},1160:{items:4}}
    });
    
    $(".accordian-group .filter-list").hide(),$(".accordian-group .filter-title").click(function(a){$(".accordian-group .filter-list").slideUp(),$(".accordian-group .filter-title span i").removeClass("fa-minus").addClass("fa-plus"),1==$(this).next(".filter-list").is(":hidden")&&($(this).next(".filter-list").slideDown("100"),$(this).find("span i").addClass("fa-minus"))});

    // $(".progress .progress-bar").css("width",function(){return $(this).attr("aria-valuenow")+"%"}),$(".dobpicker").datepicker({format:"yyyy-mm-dd"}).on("change",function(){$(".datepicker").hide()});
    $(".close-search").on("click",function(){$("#search-field").fadeOut()});

    $(".inner-tabs li").on("click",function(){let a=$(this).data("role");return $(".inner-tabs li").removeClass("active"),$(this).addClass("active"),$(".all").fadeOut(),$("."+a).fadeIn(),console.log(a),!1}),$(".banner-video-link").on("click",function(){const a=function(e){const a=e.match(/^.(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]).*/);return a&&11===a[2].length?a[2]:null}($(this).data("link")),i="https://www.youtube.com/embed/"+a;$(".video-banner-src").attr("src",i),console.log("Video ID:",a),console.log("Video ID:",i)}),$(".video-banner-close").on("click",function(){$(".video-banner-src").attr("src","")});

    $(".tab-header ul li").on("click",function(){$(".tab-header ul li").removeClass("active"),$(this).addClass("active");var a=$(this).find("a").attr("href").slice(1);return $(".tab-panel").hide(),$("."+a).fadeIn(),console.log(a),!1}),$(".tab-header ul li a").hover(function(){$(".tab-header ul li a").css({"border-bottom-color":"transparent"}),$(this).css({"border-bottom-color":"#0c5ca8","z-index":"3"})},function(){$(".tab-header ul li a").css({"border-bottom-color":"transparent"}),$(".tab-header ul li.active a").css({"border-bottom-color":"#0c5ca8","z-index":"3"})});

    var n=$("#sync1"),l=$("#sync2"),r=!0;n.owlCarousel({items:1,slideSpeed:2e3,nav:!0,autoplay:!1,dots:!1,loop:!1,responsiveRefreshRate:200,navText:["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]});


});


