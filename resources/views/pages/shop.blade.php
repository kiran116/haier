@extends('layouts.appmaster')
@section('content')
<section class="show-banner static-slider">
    <div class="container-fluid">
    <div class="row">
            <div class="col-12">
                <?php
                if ($result_home_banner->num_rows > 0) { ?>
                    <div class="">
                        <?php
                        while ($row = $result_home_banner->fetch_assoc()) {
                            if ($row['c_bnr_type_id'] == 4 || $row['c_bnr_type_id'] == 10) { ?>
                                <a href=" <?php echo $row['c_bnr_redirect_link'] != '' ? $row['c_bnr_redirect_link'] : '' ?> ">
                                    <img src="<?php echo $row['c_bnr_img'] != '' ? $row['c_bnr_img'] : 'images/banner/850x250.png' ?>" alt="slider" class="desktop-banner">
                                </a>
                            <?php }
                            if ($row['c_bnr_type_id'] == 5 || $row['c_bnr_type_id'] == 11) { ?>
                                <a href=" <?php echo $row['c_bnr_redirect_link'] != '' ? $row['c_bnr_redirect_link'] : '' ?> ">
                                    <img src="<?php echo $row['c_bnr_img'] != '' ? $row['c_bnr_img'] : 'images/banner/480x250.png' ?>" alt="slider" class="mobile-banner">
                                </a>
                        <?php }
                        } ?>
                    </div>
                <?php  } ?>
            </div>
        </div>
    </div>
</section>
@endsection