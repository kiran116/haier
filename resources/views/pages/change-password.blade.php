@extends('layouts.appmaster')
@section('content')
<section class="show-banner">
     <div class="banner-img">
         <img src="{{asset ('public/images/haier/banners/registration.png')}}" alt="">
     </div>
</section>

<div class="container">
     <div class="row forms">
         <div class="col-lg-4 col-lg-offset-4 personal-details">
             <div class="row ">
                 <div class="about-para">
                     <div class="page-title">Change Password</div>
                 </div>
                 <div class="col-md-12 col-xs-12" id="">
                     <div class="alert login-alert hidden">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: 16px;margin-right: 7px;">&times;</a>
                         <strong id="login_msg"> </strong>
                     </div>
                     <form action="#" class="sign-up" method="post" id="ChangepwdForm">
                         <div class="row">
                             <div class="col-md-12 form-group">
                                 <label for="oldpwd" class="input-labels">Old Passowrd</label>
                                 <label for="oldpwd" class="field prepend-icon">
                                     <input type="password" name="oldpwd" id="oldpwd" class="form-control" minlength="6">
                                     <label for="oldpwd" class="field-icon"></label>
                                 </label>
                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-12 form-group">
                                 <label for="newpwd" class="input-labels">New Passowrd</label>
                                 <label for="newpwd" class="field prepend-icon">
                                 <input type="password" name="password" id="password" class="form-control"  minlength="6">
                                     <label for="newpwd" class="field-icon"></label>                                        
                                     <span id="StrengthDisp" class="badge displayBadge"></span>
                                </label>
                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-6 form-group update-pwd">
                                 <button type="submit" class="btn btn-plane ruby-color update-btn">Update</button>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
     </div>
 </div>
@endsection