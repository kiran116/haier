@extends('layouts.appmaster')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px">
            <div class="about-para pl-0">
                <div class="page-title">Terms & Conditions</div>
            </div>
            <div class="terms-para">
                <ul>
                    <li>
                        <p>
                            <a href="https://shop.haierindia.com/">shop.haierindia.com</a> is an online shopping portal provided by Haier Appliances India Private Limited (hearinafter called as Haier) for the range of Appliances in India. The terms and conditions contained herein along with the Privacy Policy and Agreement regulating our relationship regarding the use/access of by you. You are advised to read this Agreement carefully before entering into a transaction on <a href="https://shop.haierindia.com/">shop.haierindia.com</a>. If you accept this agreement, it will be a legally binding agreement between you and Brandbuddiez Technologies Pvt. Ltd. (hereafter “BBTPL”)
                        </p>
                    </li>
                    <li>
                        <p>
                            Your visit to this website is subject to the Payment, Delivery and Returns Policy and Terms and
                            Conditions available here on this website.
                        </p>
                    </li>
                    <li>
                        <p>
                            This website <a href="https://shop.haierindia.com/">shop.haierindia.com</a> is owned by Haier Appliances India Private Limited an India
                            based company incorporated under the Indian Companies Act of 1956 and operated by
                            Brandbuddiez Technologies Pvt. Ltd. (hereafter “BBTPL”). The brand 'HAIER' belongs to Haier
                            Appliances India Private Limited. and is being used on this site under a specific agreement. By
                            placing an order on this site, you are entering into a purchase/sale transaction with “BBTPL”).
                            The money will be collected by BBTPL. This applies to all cases where the payment is made by
                            modes i.e., all credit card, bank card, debit card, cheque, bank transfer and other payments are
                            to be made to <a href="https://shop.haierindia.com/">shop.haierindia.com</a>
                        </p>
                    </li>
                    <li>
                        <p>
                            our credit card or debit card statements will reflect Brandbuddiez Technologies Pvt. Ltd. In case
                            of third-party statements including bank and credit card statements, the merchant’s name may
                            appear in an abbreviated format.
                        </p>
                    </li>
                    <li>
                        <p>
                            To place an order, you will need to complete the transaction on the website. This may or may
                            not be assisted with a phone call with the customer service representative. By placing an order
                            on the site or on phone, you are agreeing to the terms and conditions and payment policy
                            published in the appropriate section of the website.
                        </p>
                    </li>
                    <li>
                        <p>
                            After you have made your selection and added products to your shopping cart, you will need to
                            proceed to the checkout section.
                        </p>
                    </li>
                    <li>
                        <p>
                            this section, you will be asked for address and other contact information as well as payment related information
                        </p>
                    </li>
                    <li>
                        <p>
                            Once the payment transaction has been successfully completed, your order will be processed
                            on receipt of the funds from the bank or Credit Card Company. It is at this stage that the sale is
                            said to be complete. Thereafter your products will be shipped based on product availability and
                            completion of logistics operations related to the same.
                        </p>
                    </li>
                </ul>
                <h5>The terms and conditions are enumerated as follows:</h5>
                <ul class="roman">
                    <li>
                        <h4>Definitions:</h4>
                        <div class="inner-wrap">
                            <ul class="numeric">
                                <li>
                                    <p>"Agreement" means the terms and conditions as detailed herein including all schedules,
                                        appendices, annexure, privacy policy and will include the references to this Agreement as
                                        amended, supplemented, varied or replaced time to time. </p>
                                </li>
                                <li>
                                    <p>
                                        <a href="https://shop.haierindia.com/">shop.haierindia.com</a> shopping portal owned by Haier Appliances India Private Limited and
                                        operated by BBTPL which provides a platform to the shoppers to buy the products listed on the
                                        site.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        "Vendor"/"Seller"/” Affiliate” shall mean Brandbuddiez Technologies Pvt. Ltd. (“BBTPL”)a legal entity incorporated under the Indian Companies Act, 1956 and is offering for sale the products on <a href="https://shop.haierindia.com/">shop.haierindia.com</a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        "Customer" / "Buyer" shall mean the person or any legal entity who accepts the offer for sale on-site by placing an order for and or purchases any products offered for sale on the site & make payment of product in full.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        "User"/ "You" means and includes you and/or any person or an entity including Vendor/Seller/Affiliate using or accessing the services provided on this Site.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <h4>Eligibility:</h4>
                        <p>You represent and warrant that you are competent and eligible to enter into a legally binding agreement and have the requisite authority to bind the other party to this Agreement. You shall not use this Site if you are not competent to contract under the applicable laws, rules and regulations including but not limited to Indian Contracts Act.</p>
                    </li>
                    <li>
                        <h4>Term:</h4>
                        <p>This Agreement shall continue to be in full force and effect for so long as you continue using the site. The site provides the online shopping platform wherein the Users can purchase/buy the products and services listed on it pursuant to the terms and conditions set forth below. By clicking on the “Pay now” button, you are agreeing to use the Services in a manner consistent with and abide by the terms and conditions of this Agreement, our Privacy Policy, and with all applicable laws and regulations.</p>
                    </li>
                    <li>
                        <h4>Termination:</h4>
                        <p>Either User/ You or “BBTPL” may terminate the agreement at any time, with or without cause. However, “BBTPL” reserves the right, in its sole discretion, to terminate your access to the products and services offered on the site or any portion thereof at any time, without notice.</p>
                    </li>
                    <li>
                        <h4>Modification of Terms and Conditions:</h4>
                        <p>“BBTPL” may at any time modify the terms and conditions contained on the Site without any prior notification to you. You can access the latest version of the Terms and Conditions at any given time. You should regularly review the Terms and Conditions. In the event the modified Terms and Conditions are not acceptable to you, you should discontinue using the site. However, if you continue to use the site, you agree to accept and abide by the modified Terms and Conditions.</p>
                    </li>
                    <li>
                        <h4>Online Shopping Platform:</h4>
                        <p>
                            You further agree and undertake that you are accessing the services available on this Site and transacting at your sole risk and are using your best and prudent judgment before entering into any transaction through this Site.
                        </p>
                        <p>
                            Neither “BBTPL” nor Haier accepts no liability for any errors or omissions, whether on behalf of itself or third parties.
                        </p>
                        <p>
                            The address at which delivery of the product ordered by you is to be made should be correct and accurate in all respect.
                        </p>
                        <p>
                            After the confirmed receipt of payment, “BBTPL”)shall arrange for the delivery of the product to the recipient at the shipping address provided by the Buyer.
                        </p>
                        <p>
                            Before placing an order, you are advised to check the product description carefully. By placing an order for any product, you agree to be bound by the terms & conditions of sale included in the item's description thereon.
                        </p>
                    </li>
                    <li>
                        <h4>Product Pricing:</h4>
                        <p>
                            Products are listed on the websites with their current pricing and while all the necessary cautions have been adhered to label the products accurately , there can be a case of errors at the data entry stage or /and updating stage may occur. “BBTPL” reserves the sole right, without informing the buyer , for cancelation of such order in case a transaction has been made, wherein the price indicated is not correct Prices shall always be subject to change without any advance notice to buyer . All prices on the web site are given in Indian Rupees. For international cards used on the site, the bank's applicable exchange rate and charges shall apply. We have no jurisdiction on this and any disputes or queries on exchange rates and bank charges need to be directed to the bank or institution that issued your card or payment instrument. All orders shall be acknowledged at current pricing. We will bill at price in effect at the time of receipt of money and generating an invoice. Our products shall attract GST as per applicable provisions in India.

                        <p>The price mentioned on the website are inclusive of all the taxes. A breakup of taxes will be provided in the Invoice.</p>
                        </p>
                    </li>
                    <li>
                        <h4>Security: </h4>
                        <p>
                            Transactions on the website are protected by SSL (Secure Sockets Layer) and Secure Data Encryption using a 1024-bit process. Any information you enter when transacting with the website is sent in a Secure Socket Layer (SSL) session and is encrypted to protect you against unintentional disclosure to third parties. This is an assurance that it follows the best security practices adopted by major online vendors where all payments are processed in real-time for your security and immediate peace of mind.
                        </p>
                        <p>
                            Credit card and Debit card information is not stored by us and is not taken by us. This information is taken directly by the payment gateway provided who is authorized and is compliant with the regulations and requirements of various banks and institutions and payment franchisees that it is associated with. All and/or any details stored by you on the web browser including but not limited your name, age, residential address, delivery address, card details, PIN, password etc. are out of our liability and jurisdiction and hence you should take utmost care while furnishing these details and accepting any request to save such data on web browsers.
                        </p>
                    </li>
                    <li>
                        <h4> Out of Stock situations:</h4>
                        <p>Our endeavour is to ensure that all products listed on the site are available in stock for dispatch to the buyer. However, in rarest of cases , if the same is not available for any reason, we will contact you within seven working days and provide you an option to exchange, delay or cancel the order based on your convenience.</p>
                    </li>
                    <li>
                        <h4>Payment Options:</h4>
                        <p>The following modes of payments are available:</p>
                        <ul class="numeric">
                            <li>
                                <p>Domestic and international credit cards issued by banks and institutions that are part of the Visa, Master Card & Amex Card</p>
                            </li>
                            <li>
                                <p>Visa and MasterCard Debit cards</p>
                            </li>
                            <li>
                                <p>
                                    Net banking/Direct Debit payments from select banks in India. A full list is available at the time of Check Out and prior to making payments for purchases
                                </p>
                            </li>
                        </ul>
                        <p>
                            As prescribed by the financial institutions issuing the credit or debit cards affiliated with Visa and MasterCard you will require to submit your 16-digit credit card number, card expiry date and 3-digit CVV number (usually on the reverse of the card) when you make your online transaction using your Credit or Debit card. You should also have enrolled your Credit Card with VBV (Verified by Visa) or MSC (MasterCard Secure Code) to complete the transaction.
                        </p>
                        <p>
                            Please note that Goods & Service tax may be levied on Interest by the issuing bank on the EMI


                        </p>
                        <p>
                            To place an order, you will need to complete the transaction on the website. This may or may not be assisted with a phone call with the customer service representative. By placing an order on the site or on phone, you are agreeing to the terms and conditions and payment policy published in the appropriate section of the website or affiliated websites where specifically referred to such affiliated websites.


                        </p>
                    </li>
                    <li>
                        <h4>Shipment of Products:</h4>
                        <p>
                            The delivery of the products shall be made on or within 15 days excluding public holidays and only after receipt of the payment from the relevant Bank or Credit Card Company, delivery of the product shall be made subject to the availability of the products. All deliveries
                        </p>
                        <p>
                            will be made after date and time confirmation with the customer. There shall be no Delivery charge during the sale of the products. Goods will need to be signed for upon delivery. We take no responsibility for goods delivered incorrectly due to an error in the information provided by You or signed by an alternative person other than the person ordering the product at the address indicated at the time of the order. Since the transactions are authorized by the cardholder, we do not take responsibility for incorrect addresses provided at the time of placing the order
                        </p>
                        <p>
                            The delivery will NOT be redirected/redelivered to any other address in any circumstance. The selected product will be delivered only in the city it is mentioned as “available” on this Site. “BBTPL” shall not be held responsible for damage of products after delivery.
                        </p>
                        <p>“BBTPL” will consider the order delivery “failed” in the following cases:</p>
                        <ul>
                            <li>
                                <p>Delivery not done due to the wrong address.</p>
                            </li>
                            <li>
                                <p>Delivery not done due to the wrong address.</p>
                            </li>
                            <li>
                                <p>Premises locked.</p>
                            </li>
                            <li>
                                <p>In case of floods/ Heavy Rains/ National Bandh or any other Act of Good or Government restrictions hindering the timely delivery, we hereby reserve the right to reschedule the delivery to another date. In case the product is not delivered due to any of the above reasons, we will inform you about the same. All claims for shortages or damages must be reported to customer service on the day of delivery through the contact us page on the web-store. Also, the said shortage of goods needs be highlighted and signed on the delivery challan copy and should be returned to the same delivery person.</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h4>Product returns</h4>
                        <p>Orders on <a href="https://shop.haierindia.com/">shop.haierindia.com</a> can only be cancelled before they are packaged for shipping. Once products are out for delivery from our warehouse or delivered to you and accepted as accurate and as per the original order, there is no refund or exchange possible.</p>
                        <p>
                            Customer should contact their issuer bank if the order cancelled was placed on EMI. Any interest or Closure fees assessed on EMI cancellation will not be borne by “BBTPL”.
                        </p>
                        <p>
                            If the order which has been cancelled was placed at a special price, under cash back, exchange, upgrade or any other offer, the same offer benefit can be availed on a new transaction only post successful processing of refund. The new transaction should also meet all offer terms to be eligible for offer benefits again.
                        </p>
                        <p>
                            In a rare case if a cancelled order is still delivered, please raise a return request within 24 hours via mail on <a href="mailto:in.eshop@haierindia.com">in.eshop@haierindia.com</a> Return will be taken only if product is not activated and not used with the Brand Seal intact.
                        </p>
                        <p>
                            To return any items that are damaged or not as per your original order, please return at the time of delivery to the delivery person or Inform our customer care within 24 hours from the time of the product delivery.
                        </p>
                        <p>
                            All damaged items or items not as per your order that are to be returned must be accompanied by a copy of the original receipt and in the same condition at the time of delivery, otherwise the same will not be taken back.
                        </p>
                        <p>
                            Please note that we cannot offer exchanges or refunds on any open package or used products.
                        </p>
                        <p>
                            Please allow one to three weeks from the day you return your damaged package or an item not as per your order, for your refund request to be processed. You will be contacted once your return is complete.


                        </p>
                    </li>
                    <li>
                        <h4>Please allow one to three weeks from the day you return your damaged package or an item not as per your order, for your refund request to be processed. You will be contacted once your return is complete.</h4>
                        <p>Notwithstanding other legal remedies that may be available to Haier Appliances India Private Limited, Haier Appliances India Private Limited may in its sole discretion limit User activity by immediately removing User listing either temporarily or indefinitely or suspend or terminate User membership, and/or refuse to provide User with access to the Site.</p>
                        <ul>
                            <li>
                                <p>If the User is in breach any of the terms and conditions of this Agreement;</p>
                            </li>
                            <li>
                                <p>If your actions may cause any harm, damage or loss to the other Users or Haier Appliances India Private Limited.</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h4>Indemnity:</h4>
                        <p>User agrees to defend, indemnify and hold harmless Haier Appliances India Private Limited, its employees, directors, officers, agents and their successors and assigns and against any and all claims, liabilities, damages, losses, costs and expenses, including attorney's fees, caused by or arising out of claims based upon the use of User's actions or inactions, including but not limited to any warranties, representations or undertakings or in relation to the non-fulfilment of any of its obligations under this Agreement or arising out of the User's infringement of any applicable laws, regulations including but not limited to Intellectual Property Rights, payment of statutory dues and taxes, claim of libel, defamation, violation of rights of privacy or publicity, loss of service by other subscribers and infringement of intellectual property or other rights. This clause shall survive the expiry or termination of this Agreement.</p>
                    </li>
                    <li>
                        <h4>Limitation of liability :</h4>
                        <p>To the extent permitted by law, we (including our related bodies corporate, officers, employees and agents) exclude all liability arising from your use of the Webpage any interruption of use, any bugs, viruses, malware. Except as expressly provided in these terms and conditions and to the fullest extent allowed by law in no event whether for breach of contract, warranty, negligence, strict liability in tort, or otherwise will we (including our related bodies corporate, officers, employees and agents) be liable for any losses or damages that a third party suffers, any losses or damages that a third party suffers, any losses or damages which are not foreseeable consequence of our failure to comply with these terms and conditions, any loss or damage to data, any special, incidental, or indirect damages or for any economic consequential damages of any kind, any lost profits, business revenue, goodwill or anticipated savings. This limitation and exclusion will apply even if we were informed or should have known of their possibility.</p>
                    </li>
                    <li>
                        <h4>Governing Law and Jurisdiction:</h4>
                        <p>
                            This website is controlled by “BBTPL” -, India and the laws of India shall apply. This agreement is governed and construed in accordance with the Laws of India. You hereby irrevocably consent to the exclusive jurisdiction and venue of courts in Delhi, India, in all disputes arising out of or relating to the use of the Sites. Use of the Site is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph. You agree to indemnify and hold “BBTPL”, its subsidiaries, affiliates, directors, officers and employees, harmless any claim, demand, or damage, including reasonable attorneys' fees, asserted by any third party due to or arising out of your use of or conduct on the”BBTPL”.
                        </p>
                    </li>
                    <li>
                        <h4>Intellectual Property Rights:</h4>
                        <p>
                            HAIER and other names, graphics, logos, and icons identifying HAIER or its products or services are the proprietary marks of Haier Appliances India Private Limited. These IPR’s may not be used in connection with any product or service that is not offered by Haier Appliances India Private Limited, in any manner that is likely to cause confusion among customers, or in any manner that disparages or discredits Haier Appliances India Private Limited. All other trademarks not owned by “BBTPL” that appear on this site are the property of their respective owners.


                        </p>
                    </li>
                    <li>
                        <h4>Entire Agreement:</h4>
                        <p>
                            These Terms and Conditions, together with the privacy policy, and other rules and policies posted on the site, which are hereby incorporated as set forth fully in these Terms and Conditions, constitute the entire agreement between you and “BBTPL” with respect to your use of and material available through the site, and they supersede all prior or contemporaneous communications and proposals between you and “BBTPL”with respect to this site. Any rights not expressly granted in these Terms and Conditions are reserved.</p>
                    </li>
                    <li>
                        <h4>Submissions:</h4>
                        <p>
                            “BBTPL” welcomes your comments about your experiences shopping with us and your suggestions about how to improve this site. Any comments, ideas, suggestions, initiation, or any other content you contribute to “BBTPL” or this site (including the name you submit with any content) will be deemed to include a royalty-free, perpetual, irrevocable, nonexclusive right and license for Haier Appliances India Private Limited to adapt, publish, reproduce, disseminate, transmit, distribute, copy, use, create derivative works, display worldwide, or act on such content, without additional approval or consideration, in any, media, or technology now known or later developed for the full term of any rights that may exist in such content, and you waive any claim to the contrary. You represent and warrant that you own or otherwise control all of the rights to the content that you contribute to this site and that use of your content by Haier Appliances India Private Limited will not infringe upon or violate the rights of any third party.</p>
                    </li>
                    <li>
                        <h4>Arbitration:</h4>
                        <p>
                            In case of any dispute arising out of this transaction shall be settled through Arbitration. The place of Arbitration shall be Delhi , the arbitration proceedings shall be in accordance with the rules of Arbitration and conciliation Act, 1996. The same shall be conducted by the sole arbitrator. The arbitrator shall be mutually appointed by both the parties.
                    </li>
                    <li>
                        <h4>Disclaimer:</h4>
                        <p>
                            The prices and products advertised on this site are for sale from this website only. Prices and product availability of the brand offered herein at site may vary between online site and retail store. As Prices, product descriptions and availability can change quickly; “BBTPL” does not warrant the accuracy or completeness of information on this website and will not be responsible for any typographical errors or errors in illustrations, pictures or descriptions. Pictures of products are for illustration only and actual</p>
                        <p>
                        <p>product may differ from the picture shown. Products represented on this site are not always in stock. All item descriptions, images, product logos, availability and pricing are subject to change at any time without notice. It is the responsibility of the customer to ensure that any purchase decisions are based on current information by call us at or email us at <a href="mailto:in.eshop@haierindia.com">
                                in.eshop@haierindia.com
                            </a></p>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection