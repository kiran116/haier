@extends('layouts.appmaster')
@section('content')
<section class="show-banner">
    <div class="banner-img">
        <img src="{{asset ('public/images/haier/banners/registration.png') }}" alt="">
    </div>
</section>

<div class="container">
    <div class="row about body-min-height">
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px">
            <div class="about-para">
                <div class="page-title">Order List</div>
            </div>
        </div>
        <div class="main-content-wrapper">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive table-content">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="product-thumbnail-th" style="font-weight: bold">Order No</th>
                                        <th class="product-name-th" style="font-weight: bold">Reference No</th>
                                        <th class="product-price-th" style="font-weight: bold">Payment Status</th>
                                        <th class="product-subtotal" style="font-weight: bold">Total Quantity</th>
                                        <th class="product-quantity-th" style="font-weight: bold">Total Amount</th>
                                        <th class="product-quantity-th" style="font-weight: bold">Order Status</th>
                                        <th class="product-remove" style="font-weight: bold">Details</th>
                                        <th class="product-remove" style="font-weight: bold">Download Receipt</th>
                                        <th class="product-remove" style="font-weight: bold">Download Invoice</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                  <td colspan="7">
                                       <h4>Your order list is empty.....</h4>
                                  </td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 pagination-area">
                        
                    </div>
                    <!-- End Shopping Cart Area -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection