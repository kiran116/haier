@extends('layouts.appmaster')
@section('content')

<div class="details-banner-img">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="static-slider">
                    <a href="">
                        <img src="https://s3.ap-south-1.amazonaws.com/brandbuddiez.com//23/img/banner/FI2007_AC-&-SBS-Website-Banner-1680x300px.jpg"
                            alt="slider" class="desktop-banner">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb breadcrumb-list">
                    <ul>
                        <li><span><a href="{{route('home')}}">Home</a></span></li>
                        
                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>

                        <li>
                            <span class="current">
                                <a href="javascript:void(0)">
                                    {{  isset($product) ? $product->cat_name : "" }}
                                </a>
                            </span>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>

                        <li>
                            <span class="current">
                                <a
                                    href="http://localhost/haier/shop.php?catid={{$product->cat_id;}}&ptid[]={{$product->c_pt_id; }}">
                                    {{ isset($product) ? $product->c_pt_name : "" }}
                                </a>
                            </span>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>

                        <li>
                            <span class="current">{{ isset($product) ? $product->c_prd_display_name : "Product Name" }}
                            </span>
                        </li>

                    </ul>
                </div>
                <h2 class="product-title product-name pb-0 mobile">
                    <input type="hidden" id="fbprdname"
                        value="{{ isset($product->c_prd_display_name) && $product->c_prd_display_name != '' ? $product->c_prd_display_name : 'Product'; }}">
                    <input type="hidden" id="fbprddesc"
                        value="{{ isset($product->c_prd_itemdesc) && $product->c_prd_itemdesc != '' ? $product->c_prd_itemdesc : 'Product Description'; }}">
                    {{ isset($product->c_prd_display_name) && $product->c_prd_display_name != "" ? $product->c_prd_display_name : "Product"; }}
                </h2>
            </div>
        </div>
    </div>
</div>

<div class="tab-header">
    <ul>
        <li class="active">
            <a href="#features-tab">Features</a>
        </li>
        <li>
            <a href="#specification-tab">Specifications</a>
        </li>
        <li>
            <a href="#manual-tab">Resources</a>
        </li>
        <li>
            <button class="btn btn-plane blue-color buynow" id="tool_tip1" onclick="add_to_cart();"
                title="Please Enter the Pincode" data-prd-id="" disabled>&#8377;
                - Buy Now</button>
        </li>
    </ul>
</div>

<div class="product-tab">
    <div class="tab-panel features-tab">
        <div class="container-fluid product-details-wrap">
            <div class="row product-details-cont">
                <div class="col-lg-12 product-details-wrap">
                    <div class="col-lg-5 sticky-wrap">
                        <div class="new-product-gallery">
                            <div id="sync1" class="owl-carousel">
                                <!--  -->
                                <div class="item">
                                    <div class="new-product">
                                        <img class="zoomLs" src="{{$product_image->c_prd_img_name}}"
                                            alt="{{$product_image->c_prd_display_name }}">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="new-product">

                                    </div>
                                </div>
                                <div role="item">
                                    <img src="{{ asset('public/images/products/17.jpg')}}" alt="gallery">
                                </div>
                            </div>

                            <div id="sync2" class="owl-carousel owl-thumbnail">

                                <div class="item">
                                    <div class="new-product">
                                        <img src="{{$product_image->c_prd_img_name}}"
                                            alt="{{$product_image->c_prd_display_name}}">
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="new-product">
                                        <img src="{{$product_image->c_prd_img_name}}"
                                            alt="{{$product_image->c_prd_display_name}}">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7 product-area-wrap">
                        <div class="product-revier-area">
                            <h2 class="product-title product-name pb-0">
                                <input type="hidden" id="fbprdname"
                                    value="{{ isset($product->c_prd_display_name) && $product->c_prd_display_name != '' ? $product->c_prd_display_name : 'Product'; }}">
                                <input type="hidden" id="fbprddesc"
                                    value="{{ isset($product->c_prd_itemdesc) && $product->c_prd_itemdesc != '' ? $product->c_prd_itemdesc : 'Product Description'; }}">
                                {{ isset($product->c_prd_display_name) && $product->c_prd_display_name != "" ? $product->c_prd_display_name : "Product"; }}
                            </h2>
                            <div class="product-desc-wrap">

                                <div class="product-short-desc text-left">
                                    <p>
                                        {{ isset($product->c_prd_itemdesc) ? $product->c_prd_itemdesc : "Product Description" }}
                                    </p>
                                </div>

                                <div class="product-info-warranty">

                                    <div class="product-info-panel">
                                        <div class="product-row key-feature">
                                            <div class="key-features-wrap">

                                                <div class="product-label">Key Features</div>
                                                <ul class="keyft feature-list">
                                                    <li>
                                                        <div class="detail_inlineblock">
                                                            <img src="" class="card-img-top" alt="Feature">
                                                        </div>
                                                        <div class="detail_inlineblock">
                                                            <span></span>
                                                        </div>
                                                    </li>
                                                </ul>

                                            </div>

                                            <div class="product-prices product-row">
                                                <div class="price-info-wrap">
                                                    <div class="price-info">
                                                        <div class="product-label">MRP</div>
                                                        <del>
                                                            <span class="our-price"><i class="fa fa-inr"
                                                                    aria-hidden="true"></i>
                                                            </span>
                                                        </del>
                                                        <p>(Inclusive of all taxes)</p>
                                                    </div>
                                                    <div class="price-info">
                                                        <div class="product-label">Offer Price</div>
                                                        <span class="our-price base-price">
                                                            <i class="fa fa-inr" aria-hidden="true"></i></span>
                                                        <small>(%
                                                            off)</small>

                                                        <p>(Inclusive of all taxes)</p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="product-warranty-panel">
                                                <div class="buy-extended-warranty">
                                                    <div class="panel-header">
                                                        <h5>Add Additional Protection Plan from Haier Appliances</h5>
                                                        <!-- <span>Secure Payment | Original Product</span> -->
                                                    </div>
                                                    <div class="panel-body">

                                                        <div class="eligibilty">
                                                            <div class="product-row">
                                                                <strong>Enter product purchase date to check
                                                                    eligibility</strong>
                                                                <form action="javascript:void(0)"
                                                                    class="form-inline form-warranty" id="purchaseform">
                                                                    <div class="coupon-code-field">
                                                                        <input type="date"
                                                                            placeholder="Date of purchase"
                                                                            class="form-control input-lg "
                                                                            name="purchase_date" id="purchase_date"
                                                                            required
                                                                            value="<?php echo isset($_COOKIE['purchasedate']) ? $_COOKIE['purchasedate'] : "" ?>">
                                                                        <!-- <input type='hidden' id="prditemcode" value=<?php echo $product['c_prd_itemcode']; ?>> -->
                                                                        <input value="submit" type="submit"
                                                                            class="btn btn-plane blue-color check-btn">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>

                                                        <ul>



                                                            <li class="form-group">
                                                                <input type="radio" id="warrenty_" data-warrentyid=""
                                                                    class="warrenty-check" name="warrenty">
                                                                <label for="warrenty_">
                                                                    Year Protection Plan for <a href="#">₹
                                                                    </a>
                                                                </label>
                                                            </li>


                                                            <li class="form-group warranty-prior">
                                                                <input type="radio" id="warrenty_" data-warrentyid=""
                                                                    class="warrenty-check" name="warrenty" disabled>
                                                                <label for="warrenty_">
                                                                    Year Protection Plan for <a href="#">₹
                                                                    </a></label>
                                                            </li>


                                                            <li class="form-group">
                                                                <input type="radio" id="warrenty_" data-warrentyid=""
                                                                    class="warrenty-check" name="warrenty">
                                                                <label for="warrenty_">
                                                                    Year Protection Plan for <a href="#">₹
                                                                    </a></label>
                                                            </li>

                                                        </ul>
                                                        <div class="product-prices product-row">

                                                            <div class="product-aval">
                                                                <div class="product-label">Availability</div>
                                                                <h6 class="color-red ">Coming Soon</h6>
                                                            </div>

                                                            <div class="btn-wrap">
                                                                <button class='btn btn-plane grey-color' disabled>
                                                                    <i class="fa fa-shopping-cart" aria-hidden="true">
                                                                    </i> Buy Now</button>

                                                                <button type="submit" class="btn btn-plane blue-color"
                                                                    data-toggle="modal" data-target="#bookmodel"><i
                                                                        class="fa fa-list-ul" aria-hidden="true"></i>
                                                                    Enquire Now</button>
                                                            </div>
                                                            <div class="btn-wrap">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="tab-panel specification-tab">

    <div class="container-fluid specification-container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="key-features">
                    @if(count($sql_splcat) > 0)
                    @foreach($sql_splcat as $row)
                    <h4 class="spec-list-title">{{ $row->c_spf_name }}</h4>
                    <ul class="spec-list-wrap">
                        <li class="list-data">
                            <p scope="col">hdfhshfsj</p>
                            <p scope="col">dkhshsf 1 fshhfs</p>
                        </li>
                    </ul>
                    @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel manual-tab">
    <div class="container pb-20 pt-20">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <ul class="inner-tabs">
                    <li data-role="manuals" class="active"><a href="#">User manuals</a></li>

                    <li data-role="videos" class="">
                        <a href="#">Video </a>
                    </li>
                </ul>

                <div class="all manuals">
                    <div class="col-sm-3">
                        <div class="user-manual-title">
                            <h2>User Guides</h2>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="user-manual-details">
                            <table>
                                <tr>
                                    <th>File Name</th>
                                    <td></td>
                                </tr>

                                <tr>
                                    <th>Update Date</th>
                                    <td>
                                    </td>
                                </tr>

                            </table>
                            <a class="downloadBtn" target="_blank" download="">
                                <i class="fa fa-download"></i>
                                <span> Download</span>
                            </a>
                            <p>Disclaimer: Design, features and specifications are subject to changes without prior
                                notice.</p>
                        </div>
                    </div>
                </div>

                <div class="product-video-wrapper">
                    <div class="product-video-list">

                        <div class="product-video-item">
                            <a href="" data-link="" class="banner-video-link" data-toggle="modal"
                                data-target="#videoModal">
                                <img src="" alt="">
                                <small>
                                    <i class="fa fa-play-circle"></i>
                                </small>
                            </a>
                            <span></span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
$star_rating.on('click', function() {
    $('#ratingvalueError').html('');
    $star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
});

function submitReviewForm() {
    //alert("hie"+$('#ratingvalue').val());
    var error = false;

    let form = $('#productReviewForm')[0];
    var inpObj = document.getElementById("comment");
    //console.log(form.checkValidity());
    if (!form.checkValidity()) {
        //console.log("Invalid", inpObj.validationMessage);
        $('#commentError').html(inpObj.validationMessage);
        error = true;
    } else {
        $('#commentError').html('');
    }
    if ($('#ratingvalue').val() <= 0) {
        $('#ratingvalueError').html('Please select rating');
        error = true;
    } else {
        $('#ratingvalueError').html('');
    }
    if (!error) {
        var csrfMetaTag = document.querySelector('meta[name="csrf_token"]');
        var form_data = $('#productReviewForm').serialize();
        $.ajax({
            type: 'post',
            url: "ajaxData.php",
            data: form_data + "&&productReviewForm=productReviewForm" + "&&csrf_token=" + csrfMetaTag
                .getAttribute('content'),
            dataType: "text",
            success: function(data) {
                //console.log(data);
                //alert("Review submitted sucessfully");
                if (result = "Email Sent") {

                    document.getElementById("productReviewForm").reset();

                    $.alert({

                        title: 'Thanks',
                        content: 'Your Review submitted sucessfully.',
                        type: 'green',
                        typeAnimated: true,
                        boxWidth: '260px',
                        useBootstrap: false,
                        animation: 'scale',
                        closeAnimation: 'scale',
                        buttons: {
                            Ok: {
                                text: 'Ok',
                                btnClass: 'btn btn-plane blue-color pop-ok-btn'
                            },
                        }
                    });
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                } else {
                    $.alert({
                        title: 'Error',
                        content: 'sorry something went wrong.',
                        type: 'red',
                        typeAnimated: true,
                        boxWidth: '260px',
                        useBootstrap: false,
                        animation: 'scale',
                        closeAnimation: 'scale',
                        buttons: {
                            Ok: {
                                text: 'Ok',
                                btnClass: 'btn btn-plane blue-color pop-ok-btn'
                            },
                        }
                    });

                }


            }

        });
    }
}

jQuery(document).ready(function() {

    /* Validation of text fields (only numeric,letters ). eg. A08b
      ------------------------------------------------------------------ */
    $.validator.addMethod("phone", function(value, element) {
        return this.optional(element) || /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/.test(value);
    }, "Field must contain only letters, numbers.");

    jQuery.validator.addMethod("specialChars", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
    }, "Special characters are not allowed");

    $("#circleG").hide();
    $("#bkoffline_form").validate({
        rules: {
            name: {
                required: true,
                specialChars: true
            },
            phone: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            },
            email: {
                required: true,
                email: true
            },
            state: {
                required: true
            },
            city: {
                required: true
            },
            pincode2: {
                required: true,
                phone: true
            }
        },

        messages: {
            name: {
                required: "Please enter your  Name",
                specialChars: "Special characters are not allowed."
            },
            phone: {
                required: "Please enter Number",
                digits: "Please enter only numbers",
                minlength: "Please enter atleast 10 digits",
                maxlength: "Please enter below 10 digits"
            },
            email: "Please enter valid Email",
            state: "Please enter the State",
            city: "Please enter the City",
            pincode2: {
                required: "Please enter Pincode",
                phone: "Please enter only numbers"
            }
        },

        submitHandler: function(form) {
            $("#circleG").fadeIn();
            var csrfMetaTag = document.querySelector('meta[name="csrf_token"]');
            var emailform = {
                'product_name': $('#p_name').val(),
                'name': $('#c_name2').val(),
                'phone': $('input[name=phone2]').val(),
                'email': $('input[name=email2]').val(),
                'state': $('#statedropdown2').children("option:selected").val(),
                'city': $('#city2').children("option:selected").val(),
                'calling_time': $('#calling_time2').children("option:selected")
                    .val(),
                'pincode': $('input[name=pincode2]').val(),
                'csrf_token': csrfMetaTag.getAttribute('content')
                // 'organization':$('input[name=organization]').val()

            };
            // console.log(emailform);
            $.ajax({
                type: "POST",
                url: "./outofstockpop.php",
                async: false,
                data: emailform,
                success: function(result) {
                    console.log((result == "send"));
                    $("#circleG").fadeOut();
                    if (result == "send") {
                        $("#bookmodel").modal('hide');
                        document.getElementById("bkoffline_form").reset();
                        $.alert({
                            title: 'Thanks',
                            content: 'Thank you for your interest in this product. You will hear from us shortly.',
                            type: 'blue',
                            typeAnimated: true,
                            boxWidth: '399px',
                            useBootstrap: false,
                            animation: 'scale',
                            closeAnimation: 'scale',
                            buttons: {
                                Ok: {
                                    text: 'Ok',
                                    btnClass: 'btn btn-plane blue-color pop-ok-btn'
                                },
                            }
                        });
                    } else {
                        $.alert({
                            title: 'Error',
                            content: 'sorry something went wrong.',
                            type: 'red',
                            typeAnimated: true,
                            boxWidth: '260px',
                            useBootstrap: false,
                            animation: 'scale',
                            closeAnimation: 'scale',
                            buttons: {
                                Ok: {
                                    text: 'Ok',
                                    btnClass: 'btn btn-plane blue-color pop-ok-btn'
                                },
                            }
                        });
                    }

                }
            });
        }
    });

});
</script>
@endsection