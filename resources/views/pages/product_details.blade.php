@extends('layouts.appmaster')
@section('content')

<div class="details-banner-img">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="static-slider">
                    <a href="">
                        <img src="https://s3.ap-south-1.amazonaws.com/brandbuddiez.com//23/img/banner/FI2007_AC-&-SBS-Website-Banner-1680x300px.jpg"
                            alt="slider" class="desktop-banner">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb breadcrumb-list">
                    <ul>
                        <li><span><a href="{{url('/')}}">Home</a></span></li>

                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>

                        <li>
                            <span class="current">
                                <a href="javascript:void(0)">
                                    {{  isset($product) ? $product->cat_name : "" }}
                                </a>
                            </span>
                        </li>
                        
                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>

                        <li>
                            <span class="current">
                                <a>
                                    {{ isset($product) ? $product->c_pt_name : "" }}
                                </a>
                            </span>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>

                        <li>
                            <span class="current">{{ isset($product) ? $product->c_prd_display_name : "Product Name" }}
                            </span>
                        </li>
                    </ul>
                </div>

                <h2 class="product-title product-name pb-0 mobile">
                    <input type="hidden" id="fbprdname"
                        value="{{ isset($product->c_prd_display_name) && $product->c_prd_display_name != '' ? $product->c_prd_display_name : 'Product'; }}">
                    <input type="hidden" id="fbprddesc"
                        value="{{ isset($product->c_prd_itemdesc) && $product->c_prd_itemdesc != '' ? $product->c_prd_itemdesc : 'Product Description'; }}">
                    {{ isset($product->c_prd_display_name) && $product->c_prd_display_name != "" ? $product->c_prd_display_name : "Product"; }}
                </h2>

            </div>
        </div>
    </div>
</div>

<div class="tab-header">
    <ul>
        <li class="active">
            <a href="#features-tab">Features</a>
        </li>

        <li>
            <a href="#specification-tab">Specifications</a>
        </li>

        <li>
            <a href="#manual-tab">Resources</a>
        </li>

        <li>
            <button class="btn btn-plane blue-color buynow" id="tool_tip1" onclick="add_to_cart();"
                title="Please Enter the Pincode" data-prd-id="" disabled>&#8377;{{$product->c_price_tot}}
                - Buy Now</button>
        </li>        
    </ul>
</div>

<!--maai part -->
<div class="product-tab">
    <div class="tab-panel features-tab">
        <div class="container-fluid product-details-wrap">
            <div class="row product-details-cont">
                <div class="col-lg-12 product-details-wrap">
                    <div class="col-lg-5 sticky-wrap">
                        <div class="new-product-gallery">
                            <div id="sync1" class="owl-carousel">
                                <!--  -->
                                <div class="item">
                                    <div class="new-product">
                                        <img class="zoomLs" src="{{$product->c_prd_img_name}}"
                                            alt="{{$product->c_prd_display_name }}">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="new-product">

                                    </div>
                                </div>
                                <div role="item">
                                    <img src="{{ asset('public/images/products/17.jpg')}}" alt="gallery">
                                </div>
                            </div>

                            <div id="sync2" class="owl-carousel owl-thumbnail">

                                <div class="item">
                                    <div class="new-product">
                                        <img src="{{$product->c_prd_img_name}}" alt="{{$product->c_prd_display_name}}">
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="new-product">
                                        <img src="{{$product->c_prd_img_name}}" alt="{{$product->c_prd_display_name}}">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7 product-area-wrap">
                        <div class="product-revier-area">
                            <h2 class="product-title product-name pb-0">
                                <input type="hidden" id="fbprdname"
                                    value="{{ isset($product->c_prd_display_name) && $product->c_prd_display_name != '' ? $product->c_prd_display_name : 'Product'; }}">
                                <input type="hidden" id="fbprddesc"
                                    value="{{ isset($product->c_prd_itemdesc) && $product->c_prd_itemdesc != '' ? $product->c_prd_itemdesc : 'Product Description'; }}">
                                {{ isset($product->c_prd_display_name) && $product->c_prd_display_name != "" ? $product->c_prd_display_name : "Product"; }}
                            </h2>
                            <div class="product-desc-wrap">

                                <div class="product-short-desc text-left">
                                    <p>
                                        {{ isset($product->c_prd_itemdesc) ? $product->c_prd_itemdesc : "Product Description" }}
                                    </p>
                                </div>

                                <div class="product-info-warranty">

                                    <div class="product-info-panel">
                                        <div class="product-row key-feature">
                                            <div class="key-features-wrap">

                                                <div class="product-label">Key Features</div>
                                                <ul class="keyft feature-list">
                                                    @foreach($keyfeatures as $keyfeature)
                                                    <li>
                                                        <!-- <div class="detail_inlineblock">
                                                                <img src="https://crmqa.brandbuddiez.com{{ $keyfeature->c_product_img_path_s}}{{$keyfeature->c_product_image_s}}"
                                                                    class="card-img-top" alt="Feature">
                                                            </div> -->
                                                        <div class="detail_inlineblock">
                                                            <span>{{$keyfeature->c_prdfeat_name}}</span>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>

                                            </div>

                                            <div class="product-prices product-row">
                                                <div class="price-info-wrap">
                                                    @if ($product->c_price_mrp != $product->c_price_tot)
                                                    <div class="price-info">
                                                        <div class="product-label">MRP</div>
                                                        <del>
                                                            <span class="our-price"><i class="fa fa-inr"
                                                                    aria-hidden="true"></i>
                                                                {{$product->c_price_mrp}}
                                                            </span>
                                                        </del>
                                                        <p>(Inclusive of all taxes)</p>
                                                    </div>

                                                    <div class="price-info">
                                                        <div class="product-label">Offer Price</div>
                                                        <span class="our-price base-price">
                                                            <i class="fa fa-inr"
                                                                aria-hidden="true"></i>{{$product->c_price_tot}}</span>
                                                        <small>(%
                                                            off)</small>

                                                        <p>(Inclusive of all taxes)</p>
                                                    </div>
                                                    @endif
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- bottom add to cart button code -->
                                <div class="btn-wrap">

                                    <button class='btn btn-plane blue-color buynow' id="tool_tip2"
                                        onclick="add_to_cart();" data-prd-id="" title="Please Enter the Pincode">Buy
                                        Now</button>

                                    <form action="{{route('addtocart')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="c_prd_id" value="{{$product->c_prd_id}}">
                                        <button class='btn btn-outline blue-color addtocart' id="tool_tip3">Add to Cart</button>
                                        <!-- <button type="button" class="btn btn-outline blue-color ask-an-expert" data-toggle="modal" data-target="#asktheexpertModal">Ask an expert</button> -->
                                    </form>

                                    <button type="button" class="btn btn-outline blue-color ask-an-expert"
                                        data-toggle="modal" data-target="#knowmore">Know More</button>
                                </div>
                                <!--end add to cart and buy now button -->

                            </div>

                        </div>
                    </div>

                </div>
                
                <!--add bottom brief feature in product details page -->
                <section class="key-features desktop product-feature">
                    @foreach($keyfeatures as $keyfeature)
                    @if (!empty($keyfeature))
                    <div class="row">
                        <div class="feature-wrap">
                            <div class="feature-image">
                                <img src="https://crmqa.brandbuddiez.com/{{$keyfeature->c_product_img_path_f}}{{$keyfeature->c_product_image_f }}"
                                    class="card-img-top">
                            </div>
                            <div class="feature-content">
                                @if ($keyfeature->c_prdfeat_name)
                                <h2 class="feature-description">{{ $keyfeature->c_prdfeat_name; }}</h2>
                                @endif
                                @if ($keyfeature->c_feature_description)
                                <p class="feature-description">{{ $keyfeature->c_feature_description; }}</p>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!-- <div class="feature-image">
                                <img src="{{ asset('public/images/products/17.jpg')}}" class="card-img-top" alt="Feature">
                            </div> -->
                        <!-- <div class="feature-content">
                                <p class="feature-description">Feature Not Present</p>
                            </div> -->
                    </div>

                    @endif
                    @endforeach
                </section>
                <!--end bottom features -->

                <!-- write review div start -->
                <section class="review-section new-container" id="reviewSection">
                    <div class="container">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="ml-loader" style="display:none">

                            </div>
                            <div class="write-review" id="review"><a class="btn btn-dark"
                                    href="javascript:void(0)">Write a review</a></div>
                            <form class="give-rating" id="productReviewForm">
                                <div class="row">
                                    <input type="hidden" id="prdid" name="prdid" value="" />
                                    <input type="hidden" id="prdname" name="prdname"
                                        value="{{ isset($product->c_prd_display_name) && $product->c_prd_display_name != ''  ? $product->c_prd_display_name : 'Product'}}" />
                                    <div class="col-lg-8 col-md-8 col-12 comment">
                                        <p style="font-size: 18px;">Comment</p>
                                        <textarea id="comment" name="comment" required pattern="[a-zA-Z0-9\.\s]+"
                                            minlength="30"></textarea>
                                        <p id="commentError" style="color: #e94c4c; font-size:14px !important"></p>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-12 give-rating rating">
                                        <p>Product Rating</p>
                                        <div class=" star-container star-rating">
                                            <span class="fa fa-star-o" data-rating="1"></span>
                                            <span class="fa fa-star-o unchecked" data-rating="2"></span>
                                            <span class="fa fa-star-o unchecked" data-rating="3"></span>
                                            <span class="fa fa-star-o unchecked" data-rating="4"></span>
                                            <span class="fa fa-star-o unchecked" data-rating="5"></span>
                                            <input type="hidden" id="ratingvalue" name="ratingvalue"
                                                class="rating-value" value="0">
                                        </div>
                                        <p id="ratingvalueError" style="color: #e94c4c; font-size:14px !important"></p>
                                        <?php
                                                    if (isset($_SESSION['gsslogin_user'])) {
                                                    ?>
                                        <input type="hidden" id="actualSubmit" name="review_submit" type="submit"
                                            value="Submit" class="btn btn-dark" />
                                        <input type="button" value="Submit" class="btn btn-dark"
                                            onclick="return submitReviewForm()" />
                                        <p id="review_msg" style="color: #10a000; font-size:18px !important;"></p>
                                        <?php } else { ?>
                                        <a href="login?" class="btn btn-dark" type="submit" value="Submit"
                                            hreflang="en-in">SUBMIT</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                            <div class="comment-profile" id="commentProfileId">

                            </div>
                        </div>
                    </div>
                </section>
                <!-- write review div end -->


            </div>
        </div>
    </div>
</div>

<!--end main part -->
</div>
<!-- start specification section -->
<div class="tab-panel specification-tab">
    <div class="container-fluid specification-container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="key-features">
                    @if(count($specifications) > 0)
                    @foreach($specifications as $row)
                    <h4 class="spec-list-title">{{ $row->c_spf_name }}</h4>
                    <ul class="spec-list-wrap">
                        <li class="list-data">
                            @foreach($specifications_dynamic_cols as $specifications_dynamic_col)
                            @if($specifications_dynamic_col->c_dyn_prdtype_spf_id == $row->c_spf_id)
                            <p scope="col">{{$specifications_dynamic_col->c_dyn_name}}</p>

                            @endif
                            @endforeach
                        </li>
                    </ul>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel manual-tab">
    @foreach($user_manuals as $user_manual)
    @if($user_manual->c_prd_usermanual_prd_id == $product->c_prd_id)
    <div class="container pb-20 pt-20">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">

                <ul class="inner-tabs">
                    <li data-role="manuals" class="active"><a href="#">User manuals</a></li>

                    <li data-role="videos" class="">
                        <a href="#">Video </a>
                    </li>
                </ul>

                <div class="all manuals">
                    <div class="col-sm-3">
                        <div class="user-manual-title">
                            <h2>User Guides</h2>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="user-manual-details">
                            <table>
                                <tr>
                                    <th>File Name</th>
                                    <td>{{$user_manual->c_prd_usermanual_name}}</td>
                                </tr>

                                <tr>
                                    <th>Update Date</th>
                                    <td>
                                        {{$user_manual->c_prd_usermanual_timestamp}}
                                    </td>
                                </tr>

                            </table>
                            <a href="{{route('product-info')}}" class="downloadBtn" target="_blank">
                                <i class="fa fa-download"></i>
                                <span> Download</span>
                            </a>
                            <p>Disclaimer: Design, features and specifications are subject to changes without prior
                                notice.</p>
                        </div>
                    </div>
                </div>

                <div class="product-video-wrapper">
                    <div class="product-video-list">

                        <div class="product-video-item">
                            <a href="" data-link="" class="banner-video-link" data-toggle="modal"
                                data-target="#videoModal">
                                <img src="" alt="">
                                <small>
                                    <i class="fa fa-play-circle"></i>
                                </small>
                            </a>
                            <span></span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @endif
    @endforeach
</div>

<!--end specification section -->

<!-- Modal for Video -->
<div class="modal fade video-modal" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                        allow="autoplay"></iframe>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal for Video -->
<!-- Modal for Video -->
<div class="modal fade know-more" id="knowmore" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close know-more-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="know-more-btn-container">
                    <button class="btn btn-plane blue-color service-support-btn">Service and support</button>
                    <a href="http://haierapps.haierindia.com/livedemo/" target="_blank"
                        class="btn btn-plane blue-color">Live Demo</a>
                </div>
                <div class="service-support-container">
                    <h2>For any service and support please contact</h2>
                    <ul>
                        <li><a href="https://wa.me/8553049999" target="_blank"><b> <i class="fa fa-whatsapp"></i>
                                    8553049999</b></a></li>
                        <li><a href="tel:1800-419-9999"><b class="toll_free_no"> <i class="fa fa-phone"></i>
                                    1800-419-9999 / 1800-102-9999</b></a></li>
                        <li><a href="maillto:customercare@haierindia.com"><b> <i
                                        class="fa fa-envelope"></i>customercare@haierindia.com</b></a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal for Video -->


@endsection