@extends('layouts.appmaster')
@section('content')
<section class="show-banner">
    <div class="banner-img">
        <img src="{{'public/images/haier/banners/wishlist.jpg'}}" alt="">
    </div>
</section>

<div class="container  cart-wrapper">
    <div class="col-lg-10 col-lg-offset-1">
        <div class="row about body-min-height">
            <div class="about-para login">
                <div class="page-title">My Wishlist</div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 shopping-cart ">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="pull-right">
                            <label class="continue-shopping">
                                <a href="index.php" class="wishlist-pro-title" style="cursor: pointer;">Continue Shopping</a>
                            </label>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <div class="col-md-12 personal-details pt-0">

                <!--                <div class="col-md-4 about-para1 checkout-section">
                    <div class="row pb-20">
                        
                        <div class="col-md-12 check-availability">Check availability at</div>
                        <div class="col-sm-9 col-xs-9 text-center pr-0"  style="border:px solid">
                            <input type="text" name="keyword" class="form-control input-lg" placeholder="Pincode">
                        </div>
                        <div class="col-sm-3 col-xs-3 pl-0"  style="border:px solid">
                          <input type="submit" class="btn ripple black check-btn" value="Check">
                       </div>
                       <div class="col-md-12 error-message">! error message</div>
                    </div>
                    
                    <div class="row pb-20">
                        <div class="col-md-12 check-availability">Coupan</div>
                        <div class="col-sm-9 col-xs-9 text-center pr-0"  style="border:px solid">
                          <input type="text" name="keyword" class="form-control input-lg search" >
                        </div>
                        <div class="col-sm-3 col-xs-3 pl-0">
                          <input type="submit" class="btn ripple black check-btn" value="Apply">
                       </div>
                        <div class="col-md-12 error-message">! error message</div>
                    </div>
                    
                    
                   <table class="table table-striped">
                    <tbody>
                      <tr>
                        <td>Total</td>
                        <td class="text-right fs-16"><i class="fa fa-inr"></i> 9,999</td>
                      </tr>
                      <tr>
                        <td>Shipping</td>
                        <td class="free text-right">Free</td>
                      </tr>
                      <tr>
                        <td>Grand total</td>
                        <td class="grand-total text-right"><i class="fa fa-inr"></i> 99,999</td>
                      </tr>
                    </tbody>
                  </table> 
                    
                    
                   <button type="submit" class="btn btn-default update-btn ">Check out</button>
                </div>-->
            </div>
        </div>
    </div>
</div>

@endsection