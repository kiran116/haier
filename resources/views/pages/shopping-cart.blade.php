@extends('layouts.appmaster')
@section('content')

<section class="show-banner">
    <div class="banner-img">
        <img src="{{ asset ('public/images/haier/banners/shopping-banner.jpg') }}" alt="">
    </div>
</section>
   
<div class="container cart-wrapper">
    <div class="col-lg-10 col-lg-offset-1">
        <div class="row">

            <div class="about-para login">
                <div class="page-title">Shopping Cart</div>
            </div>

            <div class="col-lg-7 col-sm-12 col-xs-12 shopping-cart">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="pull-left">
                             
                        </p>
                        <div class="pull-right">
                             <label class="continue-shopping">
                                 <a href="{{url('/')}}" class="wishlist-pro-title" style="cursor: pointer;">Continue Shopping</a>
                             </label>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="shoping-cart-wrap">
                            
                        </div>
                    </div>
                </div>
            </div>
             
        </div>
    </div>
</div>
@endsection