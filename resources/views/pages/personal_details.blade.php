@extends('layouts.appmaster')
@section('content')
<section class="show-banner">
     <div class="banner-img">
         <img src="{{asset ('public/images/haier/banners/login.png')}}" alt="">
     </div>
</section>

<div class="container">
     <div class="row about">
         <!-- <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px">
             <div class="page-title"><i class="material-icons">account_circle</i> Personal Details</div>
         </div> -->
         <div class="col-lg-8 col-lg-offset-2">
             <div class="row personal-details">
                 <div class="about-para">
                     <div class="page-title">Personal Details</div>
                 </div>
                 <div class="col-md-12 col-xs-12 update-details" id="">
                     <!-- <p class="update-details1 fw-600">Please update your details</p> -->
                     <!--<h2>Vertical (basic) form</h2>-->
                     <form action="#" class="sign-up" method="post" id="PersonaldetailForm">
                         <div class="row">
                             <div class="col-md-6 form-group">
                                 <label for="lastname" class="input-label">Name</label>
                                 <label for="fname" class="field prepend-icon">
                                     <input type="text" name="fname" id="fname" class="form-control " value="<?php echo isset($custarray) ? $custarray['cust_firstname'] : "" ?>" autocomplete="off">
                                     <label for="fname" class="field-icon"></label>
                                 </label>
                             </div>
                             <div class="col-md-6 form-group">
                                 <label for="lastname" class="input-label">Last Name</label>
                                 <label for="lname" class="field prepend-icon">
                                     <input type="text" name="lname" id="lname" class="form-control" value="<?php echo isset($custarray) ? $custarray['cust_lastname'] : "" ?>" autocomplete="off">
                                     <label for="lname" class="field-icon"></label>
                                 </label>
                             </div>
                         </div>


                         <div class="row">
                             <div class="col-md-6 form-group">
                                 <label for="mobile" class="input-label">Mobile no.</label>
                                 <label for="phone" class="field prepend-icon">
                                     <input type="text" name="phone" id="phone" class="form-control number" maxlength="10" value="<?php echo isset($custarray) ? $custarray['cust_phoneno'] : "" ?>" autocomplete="off">
                                     <label for="phone" class="field-icon"></label>
                                 </label>
                             </div>
                             <div class="col-md-6 form-group">
                                 <label for="email" class="input-label">Email id.</label>
                                 <label for="email" class="field prepend-icon">
                                     <input type="email" name="email" id="email" class="form-control email" value="<?php echo isset($custarray) ? $custarray['cust_emailid'] : "" ?>" autocomplete="off">
                                     <label for="email" class="field-icon"></label>
                                 </label>
                             </div>
                         </div>

                         <div class="row">
                             <div class='col-sm-6 form-group reg-frm-grp'>
                                 <label for="dob" class="input-label">Date of Birth</label>
                                 <div class='input-group date dobpicker'>
                                     <input type='text' class="form-control" name="dob" id="dob" readonly value="<?php echo (isset($custarray)  && $custarray['cust_dob'] != "") && ($custarray['c_fml_anndate'] != "0000-00-00" && $custarray['cust_dob'] != "1970-01-01") ? date('d/m/Y', strtotime(str_replace('/', '-', $custarray['cust_dob'])))  : "" ?>" autocomplete="off" />
                                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                     </span>
                                 </div>
                             </div>
                             <div class='col-sm-6 form-group reg-frm-grp'>
                                 <label for="anndob" class="input-label">Date of Anniversary </label>
                                 <div class='input-group date dobpicker'>
                                     <input type='text' class="form-control" name="anndob" id="anndob" readonly value="<?php echo (isset($custarray) &&  $custarray['c_fml_anndate'] != "") && ($custarray['c_fml_anndate'] != "0000-00-00" && $custarray['c_fml_anndate'] != "1970-01-01") ? date('d/m/Y', strtotime(str_replace('/', '-', $custarray['c_fml_anndate'])))  : "" ?>" autocomplete="off" />
                                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                     </span>
                                 </div>
                             </div>
                         </div>

                         <div class="row">
                             <div class="col-md-6 form-group">
                                 <label for="address" class="field prepend-icon">Address</label>
                                 <label for="address" class="field prepend-icon">
                                     <input type="text" name="address" id="address" class="  form-control" value="<?php echo isset($custarray) ? $custarray['cust_current'] : "" ?>" autocomplete="off">
                                     <label for="address" class="field-icon"><i class="fa fa-calender"></i></label>
                                 </label>
                             </div>
                         </div>

                         <div class="row">
                             <div class="col-md-6 form-group">
                                 <label for="area">Area Locality</label>
                                 <label for="area" class="field prepend-icon">
                                     <input type="text" name="area" id="area" class=" form-control" value="<?php echo isset($custarray) ? $custarray['cust_area'] : "" ?>" autocomplete="off">
                                     <label for="area" class="field-icon"><i class="fa fa-calender"></i></label>
                                 </label>
                                 <!--<input type="text" class="form-control" id="area" name="area">-->
                             </div>
                             <div class="col-md-6 form-group">
                                 <label for="landmark">Landmark</label>
                                 <label for="landmark" class="field prepend-icon">
                                     <input type="text" name="landmark" id="landmark" class=" form-control" value="<?php echo isset($custarray) ? $custarray['cust_landmark'] : "" ?>" autocomplete="off">
                                     <label for="landmark" class="field-icon"><i class="fa fa-calender"></i></label>
                                 </label>
                                 <!--<input type="text" class="form-control" id="landmark" name="landmark">-->
                             </div>
                         </div>

                         <div class="row">
                             <div class="col-md-6 form-group">
                                 <label for="city">Country</label>
                                 <label for="city" class="field prepend-icon">
                                     <select class="form-control" id="country" name="country">
                                         
                                     </select>
                                     <label for="country" class="field-icon"><i class="fa fa-calender"></i></label>
                                 </label>
                             </div>

                             <div class="col-md-6 form-group">
                                 <label for="city">State</label>
                                 <label for="city" class="field prepend-icon">
                                     <select class="form-control" id="state" name="state">
                                         
                                     </select>
                                     <label for="state" class="field-icon"><i class="fa fa-calender"></i></label>
                                 </label>
                             </div>
                         </div>

                         <div class="row">
                             <input type="hidden" id="hidden_city" name="hidden_city" value="<?php echo isset($custarray) ? $custarray['cust_city'] : "" ?>">
                             <div class="col-md-6 form-group">
                                 <label for="citylable">City</label>
                                 <label for="citylable" class="field prepend-icon">
                                     <select class="form-control" id="city" name="city">

                                         <option value="Select city"> Select city</option>
                                                          
                                              </select>
                                     <label for="citylable" class="field-icon"><i class="fa fa-calender"></i></label>
                                 </label>
                             </div>

                             <div class="col-md-6 form-group">
                                 <label for="pincode">Pincode</label>
                                 <label for="pincode" class="field prepend-icon">
                                     <input type="text" name="pincode" id="pincode" class=" form-control number" maxlength="6" value="<?php echo isset($custarray) ? $custarray['cust_pincode'] : "" ?>" autocomplete="off">
                                     <label for="pincode" class="field-icon"><i class="fa fa-calender"></i></label>
                                 </label>
                             </div>
                         </div>
                         <div class="row">
                                <div class="col-md-12 form-group">
                                <label for="password" class="field prepend-icon">Password</label>
                                    <label for="password" class="field prepend-icon">
                                        <input type="password" name="password" id="password" class="  form-control "> 
                                        
                                        <label for="password" class="field-icon"><i class="fa fa-calender"></i></label>
                                    </label>
                                </label>
                            </div>
                               
                            </div>
                         <div class="row" style="border:px solid">
                             <div class="col-md-3 form-group" style="border:px solid">
                                 <label class="container check-container additional-offer newsletter-check" style="border:px solid red">

                                 </label>
                                 <button type="submit" class="btn btn-plane ruby-color update-btn">Update</button>
                             </div>
                         </div>
                     </form>

                 </div>
                 <!-- <div class="col-md-6 about-para">
                        <div  class="row exclusive-offer-div">
                            <div class="complete-profile">
                                Complete your profile and <br/> <strong>enjoy exclusive offer</strong> <br /> from Brandbuddiez
                            </div>
                            <div class="exclusive-offer-bg">
                            </div>
                        </div>
                    </div> -->
             </div>
         </div>
     </div>
 </div>

@endsection