@extends('layouts.appmaster')
@section('content')
<section class="show-banner">
     <div class="banner-img">
         <img src="{{asset ('public/images/haier/banners/subscription.jpg')}}" alt="">
     </div>
</section>

<div class="container">
     <div class="row forms">
         <div class="col-lg-4 col-lg-offset-4 col-xs-12">
             <div class="row personal-details">
                 <div class="about-para">
                     <div class="page-title">Subscription Details</div>
                 </div>
                 <div class="col-md-12 col-xs-12 " id="aboutus">
                     <div class="alert login-alert hidden">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: 16px;margin-right: 7px;">&times;</a>
                         <strong id="login_msg"> </strong>
                     </div>
                     <form action="#" class="sign-up" method="post" id="SubscriptionForm">
                         <div class="row">
                             <div class="col-md-12 form-group">
                                 <label for="email" class="subscribe-input">Email id subscribe on</label>
                                 <label for="email" class="field prepend-icon">
                                     <input type="email" name="email" id="email" class="form-control email" value="<?php echo isset($subscriptionarray) && !empty($subscriptionarray) ? $subscriptionarray['c_subscription_emailid'] : "" ?>" placeholder="rakesh@gmail.com">
                                     <label for="email" class="field-icon"></label>
                                 </label>
                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                 <label class="container check-container additional-offer unsubscribe field prepend-icon">
                                     <span class="ml-25">Unsubscribe newsletters</span>
                                     <input type="checkbox" class="check" name="unsubscribe_chk" id="unsubscribe_chk">
                                     <span class="checkmark check-mark"></span>
                                 </label>
                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-6 form-group update-pwd">
                                 <button type="submit" class="btn btn-plane ruby-color update-btn">Update</button>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
     </div>
 </div>
@endsection