@extends('layouts.appmaster')
@section('content')
<section class="show-banner">
     <div class="banner-img">
         <img src="{{ asset('public/images/haier/banners/registration.png')}}" class="img-fluid">
     </div>
 </section>
 <div class="container">
     <div class="col-lg-10 col-lg-offset-1">
         <div class="row about">
             <div class="col-md-12">
                 <div class="carousel-title cat about-title  pb1-10">
                     <div class="about-para">
                         <div class="page-title">Contact Us</div>
                     </div>
                 </div>
             </div>
             <div class="col-md-12">
                 <div class="row">
                     <div class="col-md-6">
                         <div class="contact_us">
                             <h3>Registered Head Office</h3>
                             <p style="color:#3396ce">Haier Corporate Office </p>
                             <p>Haier Appliances India Pvt Ltd, CIN: U74140DL2003PTC119101</p>
                             <p>Building Number 1, Okhla Industrial Estate, Phase III, </p>
                             <p>Opposite Modi Mill, New Delhi-110020.</p>
                             <p><b>Telephone:</b></p>
                             <p>Landline Number : +91-11-30674000</p>
                             <!-- <p>WhatsApp : +91 8553049999</p> -->
                             <p>WhatsApp : <a href="https://wa.me/+919930055400" target="_blank"> +91 9930055400</a></p>
                             <p><b>Email :</b></p>
                             <p>For Enquiries: <a href="mailto:info@haierindia.com"> info@haierindia.com</a></P>
                             <p><b>For After-Sales Service:</b><br>
                                         <a href="https://api.whatsapp.com/send/?phone=919930055200&text=hi" target="_blank"><b>  <img src="./images/whatsapp.png" alt="whatsapp-icon" class="whatsapp-img"> 8553049999</b></a><br/>
                                         <a href="tel:1800-419-9999"><b class="toll_free_no"> <i class="fa fa-phone"></i> 1800-419-9999 </b></a><br/>
                                         <a href="tel:1800-419-9999"><b class="toll_free_no"> <i class="fa fa-phone"></i> 1800-102-9999</b></a><br/>
                                         <a href="maillto:customercare@haierindia.com"><b> <i class="fa fa-envelope"></i>customercare@haierindia.com</b></a>
                                     </p>
                         </div>
                     </div>
                     <div class="col-md-6 enquiry_form">
                         <form action="#" method="post" id="contactus">
                             <span>
                                 <h3>Enquiry</h3>
                             </span>
                             <div class="form-group">
                                 <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                             </div>
                             <div class="form-group">
                                 <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" maxlength="10" minlength="10" required>
                             </div>
                             <div class="form-group">
                                 <input type="email" class="form-control" id="email" name="email" placeholder="Email ID" required>
                             </div>
                             <div class="form-group">

                                 <textarea class="form-control" placeholder="comment" name="comment" rows="3" id="comment" required></textarea>
                             </div>
                             <input type="submit" name="submit_enquiry" class="btn btn-plane blue-color width-100">
                         </form>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
@endsection