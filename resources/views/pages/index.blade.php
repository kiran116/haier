@extends('layouts.appmaster')

@section('title','Home')
@section('content')
<div class="main-menu-area hidden-xs hidden-sm">
    <div class="navbar-scrollup container containermenu">
        <div class="row">
            <div class="col-md-12">
                <div class="menu main-menu home-one-menu">
                    <nav>
                        <ul>
                            @foreach($category as $cat)
                            <li>
                                <a href="javascript:void(0)">{{$cat->cat_name}}</a>
                                <ul class="megamenu new_menu third-mega">
                                    <li class="li-padding-top-x">
                                        <ul class="single-mega-1 thirdmg">
                                            <li>
                                                <ul class="list-unstyled width-100">
                                                    @foreach($header_product_cats as $header_product_cat)
                                                    @if($cat->cat_id == $header_product_cat->cat_id)
                                                    <li>
                                                        <a
                                                            href="shop/{{$cat->cat_name}}/{{$header_product_cat->c_pt_name}}/{{$cat->cat_id}}/{{$header_product_cat->c_pt_id}}">
                                                            <img
                                                                src="https://crmqa.brandbuddiez.com/{{($header_product_cat->c_ptype_img_path).($header_product_cat->c_ptype_img_name)}}" />
                                                            <span>{{$header_product_cat->c_pt_name}}</span>
                                                        </a>
                                                    </li>
                                                    @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            @endforeach
                            
                            <li>
                                <a href="{{route('special-offer')}}">Special Offers</a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid no-space"></div>

<div class="home-slider-wrap static-slider">
    <div class="owl-carousel owl-theme desktop-banner">
        @if (count($home_banner) > 0) 
                @foreach($home_banner as $row)
                    @if ($row->c_bnr_type_id == 1) 
                        <div class="item">
                            <a href="{{ $row->c_bnr_redirect_link != '' ? $row->c_bnr_redirect_link : '' }}">
                                <img src="{{ $row->c_bnr_img != '' ? $row->c_bnr_img : 'images/banner/850x250.png' }}" alt="slider" class="">
                            </a>
                        </div>
                    @endif
                @endforeach
        @endif
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="home-banner-info">
            <img src="https://s3.ap-south-1.amazonaws.com/brandbuddiez.com//23/img/banner/640X480.jpg"
                alt="home-info-banner" class="img-responsive mobile width-100 ">
            <img src="https://s3.ap-south-1.amazonaws.com/brandbuddiez.com//23/img/banner/1920x300.jpg"
                alt="home-info-banner" class="img-responsive desktop width-100">
        </div>
    </div>
</div>

<!--Flagship product -->
<section class="flagship-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="ff-sans title">
                    Flagship Products
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="flagship-carousal owl-carousel">
                    @foreach( $flagship_products as $product)
                    <div class="carousel-item">
                        <a href="products/{{$product->c_pt_name}}/{{$product->c_prd_display_name}}/{{$product->c_prd_id}}" class="flagship-img">
                            <img src="{{ $product->c_prd_img_name}}" alt="slider" class="img-responsive" />
                        </a>
                        <div class="flagship-details">
                            <h4>
                                <a href="products/{{$product->c_pt_name}}/{{$product->c_prd_display_name}}">
                                    {{$product->c_prd_sku}}
                                <a>
                            </h4>
                            <div class="divider"></div>
                            <div class="product_img_text1_con_center">
                                <div class="product_img_text1_con">
                                    <p class="product_img_text_p2 sd_texts" sd_key="sd_texts1">

                                    </p>
                                </div>
                            </div>
                            <a href="products/{{ isset($product->c_prd_id) ? $product->c_prd_id : '0' }}"  class="more-btn">More</a>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container carousel-container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel-title cat text-center">
                    <div class="filter-title">
                        <h2 class="ff-sans title">
                            New Arrivals
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <div class="category-wrapper owl-carousel">
                        @foreach($newly_arrival_products as $new_product)

                        <div class="list-product slider-product col-item" id="{{ isset($new_product->c_prd_id) ? ($new_product->c_prd_id) : 0}}" style="margin-top:0px;width: 100%;">
                            <div class="product-img">
                                <a href="products/{{$new_product->c_pt_name}}/{{$new_product->c_prd_display_name}}/{{$new_product->c_prd_id}}">
                                    <img class="img-responsive cat-img" src="{{ $new_product->c_prd_img_name}}" alt="slider" class="img-responsive" />
                                </a>
                            </div>

                            <div class="text-center product-details product-mob">
                                <div class="list-items" style="float:none">
                                    <h5 class="product-name cat-name1"
                                        title="{{isset($new_product->c_prd_display_name)}}">
                                        <a>
                                            {{$new_product->c_prd_display_name}}
                                        </a>
                                    </h5>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="list-feature">
                                        <ul>
                                            @foreach($product_features as $product_feature)
                                            <li>
                                                <p>{{$product_feature->c_prdfeat_name}}</p>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="list-items">
                                    <div class="btn-container">
                                        <a class="btn btn-outline blue-color" href="products/{{ isset($product->c_prd_id) ? $product->c_prd_id : '0' }}" >
                                            View Details
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container carousel-container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel-title cat text-center">
                    <div class="filter-title">
                        <h2 class="ff-sans title">
                            Best Seller
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="newly-arrival-slider-wrap">
                    <div class="owl-carousel category-wrapper">
                        @foreach($best_sellers as $best_seller)

                        <div class=" list-product slider-product col-item">
                            <div class="product-img">
                                <a href="products/{{$best_seller->c_pt_name}}/{{$best_seller->c_prd_display_name}}/{{$best_seller->c_prd_id}}">
                                    <img class="img-responsive cat-img" src="{{$best_seller->c_prd_img_name}}">
                                </a>
                            </div>
                            <div class="text-center product-details product-mob">
                                <div class="list-items" style="float:none">
                                    <h5 class="product-name cat-name1">
                                        {{$best_seller->c_prd_display_name}}
                                    </h5>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="list-feature">
                                        @foreach($product_features as $product_feature)
                                        <ul>
                                            <li>
                                                <p>{{$product_feature->c_prdfeat_name}}</p>
                                            </li>
                                        </ul>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="list-items">
                                    <div class="btn-container">
                                        <a class="btn btn-outline blue-color">
                                            View Details
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="newly-arrival-container">
    <div class="container carousel-container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel-title cat text-center">
                    <div class="filter-title">
                        <h2 class="ff-sans title">
                            Special Offer
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div>
                    <div class="category-wrapper owl-carousel">
                        @foreach($special_offers as $special_offer)

                        <div class=" list-product slider-product col-item" style="margin-top:0px;width: 100%;">
                            <div class="product-img">
                                <a
                                    href="products/{{$special_offer->c_pt_name}}/{{$special_offer->c_prd_display_name}}/{{$special_offer->c_prd_id}}">
                                    <img class="img-responsive cat-img" src="{{$special_offer->c_prd_img_name}}">
                                </a>
                            </div>
                            <div class="text-center product-details product-mob">
                                <div class="list-items" style="float:none">
                                    <h5 class="product-name cat-name1" title="{{$special_offer->c_prd_display_name}}">
                                        <a
                                            href="products/{{$special_offer->c_pt_name}}/{{($special_offer->c_prd_display_name)}}/{{isset ($special_offer->c_prd_id) ? ($special_offer->c_prd_id) : '0'}}">
                                            {{$special_offer->c_prd_display_name}}
                                        </a>
                                    </h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="list-items">
                                    <div class="btn-container">
                                        <a class="btn btn-outline blue-color"
                                            href="products/{{isset ($special_offer->c_prd_id) ? ($special_offer->c_prd_id) : '0'}}">
                                            View Details
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<div class="attentive-container">
    <div class="title-container">
        <h2>ATTENTIVE SERVICE</h2>
        <p>Have trouble using your product? Haier's service is available all year round.</p>
    </div>
    <div class="attentive-list">
        <a href="about-us" class="attentive-item">
            <!-- <img src="{{asset("public/images/haier/attentive-1.jpg")}} alt="attentive"> -->
            <img src="{{ asset('public/images/haier/attentive-1.jpg')}}" class="img-fluid">
            <article class="item-info">
                <small>About Haier</small>
                <span>World's No. 1 brand for 13 consecutive years*, dedicated to bring meaningful innovations for you,
                    inspired by you</span>
            </article>
        </a>
        <a href="http://www.haier.com/in/service-support" target="_blank" class="attentive-item">
            <!-- <img src="images/haier/attentive-2.jpg" alt="attentive"> -->
            <img src="{{ asset('public/images/haier/attentive-2.jpg')}}" class="img-fluid">
            <article class="item-info">
                <small>Service & Support</small>
                <span>
                    Haier's robust service network across the country to provide hassle-free solutions anytime, anywhere
                </span>
            </article>
        </a>
    </div>
</div>
<div class="honors-container">
    <div class="title-container">
        <h2>HAIER'S HONORS</h2>
        <p>We would like to thank you for your support which constantly spurs our persuit excellence</p>
    </div>
    <div class="honors-list">
        <div class="honors-item">
            <div class="top-img">
                <!-- <img src="{{asset('public/images/haier/std.png')}} alt=""> -->
                <img src="{{ asset('public/images/haier/std.png')}}" class="img-fluid">
            </div>
            <div class="item-count">
                <h6>50+</h6>
                <h4>International Standards</h4>
            </div>
            <div class="item-dec">
                <p>As of June 2018, it has participated in the revision of 59 international standards, leading and
                    participationg in 470 revisions of national/industry stands.</p>
            </div>
        </div>
        <div class="honors-item">
            <div class="top-img">
                <!-- <img src="{{asset('public/images/haier/worldwide.png')}} alt=""> -->
                <img src="{{ asset('public/images/haier/worldwide.png')}}" class="img-fluid">
            </div>
            <div class="item-count">
                <h6>38k+</h6>
                <h4>Patents Worldwide</h4>
            </div>
            <div class="item-dec">
                <p>Up to now, Haier has applied for more than 38,000 patents in 25 countries and regions, including
                    23,000 invention patents. These patents has been obtained either through independent R&D or from
                    overseas M&A</p>
            </div>
        </div>
        <div class="honors-item">
            <div class="top-img">
                <!-- <img src="{{('public/images/haier/award.png')}} alt=""> -->
                <img src="{{ asset('public/images/haier/award.png')}}" class="img-fluid">
            </div>
            <div class="item-count">
                <h6>170+</h6>
                <h4>Red Dot and iF Award</h4>
            </div>
            <div class="item-dec">
                <p>Up to now, Haier's high-quality products have won more than 170 world-class industrial design awards
                    such as the iF Design Award and the Red Dot Design Award, ranking first in the industry.</p>
            </div>
        </div>
    </div>
</div>
@endsection