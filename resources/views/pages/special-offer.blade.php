@extends('layouts.appmaster')
@section('content')

<style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 3px;
    background: #333;
    outline: none;
    opacity: 1 !important;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 15px;
    height: 15px;
    background: #066fb0;
    cursor: pointer;
    border-radius: 50%;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-webkit-slider-thumb2 {
    -webkit-appearance: none;
    appearance: none;
    width: 15px;
    height: 15px;
    background: #066fb0;
    cursor: pointer;
    border-radius: 50%;
}

.slider::-moz-range-thumb2 {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}
</style>

<section class="show-banner home-slider-wrap static-slider">
    @if (count($result_home_banner) > 0)
    <div class="static-slider">
        @foreach($result_home_banner as $row)
        @if ($row->c_bnr_type_id == 2)
        <div class="banner-img desktop-banner">
            <a href="javascript:void(0)">
                <img src="{{ $row->c_bnr_img != '' ? $row->c_bnr_img : 'images/banner/1680x300.jpg' }}" alt="slider"
                    class="desktop-banner">
            </a>
        </div>
        @endif

        @if ($row->c_bnr_type_id == 9)
        <div class="banner-img mobile-banner">
            <a href="javascript:void(0)">
                <img src="{{$row->c_bnr_img != '' ? $row->c_bnr_img : 'images/banner/640x300.jpg' }}" alt="slider"
                    class="mobile-banner">
            </a>
        </div>
        @endif
        @endforeach
    </div>
    @endif
</section>
<!-- Start Breadcrumb -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb breadcrumb-list">
                    <ul>
                        <li><span><a href="index.php">Home</a></span></li>
                        <li><a href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                        <li>
                            <span class="current">
                                <a href="javascript:void(0)">
                                    <?//php echo isset($categoryname) ? $categoryname : "Catlogs" ?>
                                    Special Offers
                                </a>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumb -->

<!--start left area -->
<div class="left-area">
    <div class="container" style="cursor:auto !important">
        <div class="row">
            <!-- Start Catalog -->
            <div class="col-md-3 col-sm-4 filter-sticky">
                <div class="catalog-wrapper" style="margin-top:0px">
                    <div class="mob-acc">
                        <h3 class="form-title cat-name">Special Offers
                            <a href="#" id="clearfilter" title="Clear"><i class="fa fa-refresh"
                                    aria-hidden="true"></i></a>
                        </h3>
                    </div>
                    <div class="category-filter-form">
                        <form action="javascript:void(0)">
                            <!-- NEWLY ADDED USED IN JQUERY FOR PURPOSE -->
                            <!-- <input type="hidden" name="pagenumber" id="pagenumber" value="<?php //echo isset($_GET['page']) ? $_GET['page'] : "1" 
                                                                                                ?>"> -->

                            <!--$dynmicarray-->
                            <?php //  if (isset($_GET['ptid'])) { 
                            ?>
                            <div class="categoryies-option">
                                <?php $dyncount = 0;
                                $filternamearray = array();
                                if (!empty($dynmicarray)) {
                                    foreach ($dynmicarray as $key1 => $val1) {
                                        foreach ($val1 as $key2 => $val2) { ?>
                                <div class="accordian-group">
                                    <div class="filter-title  ff-sans">
                                        <h4 class="fs-18  ff-sans"> <?php echo $key2; ?></h4>
                                        <span><i class="fa fa-plus"></i></span>
                                    </div>
                                    <div class="filter-list">
                                        <?php
                                                    sort($val2);
                                                    foreach ($val2 as $key3 => $val3) {
                                                        $dynmicchecked = "";
                                                        foreach ($val3 as $key4 => $val4) {
                                                            //when u click on checkbox it will get save into colarray and here we check key exit or not 
                                                            if (array_key_exists($key4, $colarray)) {
                                                                //if key is exist then we want to know which one is selected for that again we loop of that array
                                                                foreach ($colarray[$key4] as $colkey => $colval) {
                                                                    if ($colarray[$key4][$colkey] == $val4) {
                                                                        $dynmicchecked = "checked";
                                                                    }
                                                                }
                                                            }
                                                            $dyncount++; ?>
                                        <ul>
                                            <li class="brandname_class">
                                                <input type="checkbox" class="checker" name="dynamicol"
                                                    <?php echo $dynmicchecked; ?> id="dynamicol_<?php echo $dyncount ?>"
                                                    value="<?php echo $val4 ?>" data-col-id="<?php echo $key4; ?>"
                                                    data-prd-type="<?php echo $key1; ?>">
                                                <label class="form-label fs-14" for="dynamicol_<?php echo $dyncount ?>">
                                                    <span class="label-check"><?php echo $val4 ?></span>
                                                </label>
                                            </li>
                                        </ul>

                                        <?php } ?>
                                        <?php
                                                    }
                                                    ?>
                                    </div>
                                </div><?php
                                                }
                                            }
                                        }
                                                    ?>
                            </div>
                            <?php // if ($_GET['catid'] != 8) { ?>
                            <div class="categoryies-option price-category">
                                <div class="accordian-group">
                                    <div class="filter-title ff-sans">
                                        <h4 class="fs-18 ff-sans"> Price</h4>
                                        <span><i class="fa fa-plus"></i></span>
                                    </div>
                                    <div class="filter-list">
                                        <ul class="brandparent">
                                            <?php
                                                $maxprice = 0;
                                                $chk1 = $chk2 = $chk3 = $chk4 = $chk5 = "";
                                                if ((isset($_GET['minpricerange']) && $_GET['minpricerange'] == 0) && (isset($_GET['maxpricerange']) && $_GET['maxpricerange'] == 10000)) {
                                                    $chk1 = "checked";
                                                } else {
                                                    $chk1 = "";
                                                }
                                                if ((isset($_GET['minpricerange']) && $_GET['minpricerange'] == 10000) && (isset($_GET['maxpricerange']) && $_GET['maxpricerange'] == 20000)) {
                                                    $chk2 = "checked";
                                                } else {
                                                    $chk2 = "";
                                                }
                                                if ((isset($_GET['minpricerange']) && $_GET['minpricerange'] == 20000) && (isset($_GET['maxpricerange']) && $_GET['maxpricerange'] == 30000)) {
                                                    $chk3 = "checked";
                                                } else {
                                                    $chk3 = "";
                                                }
                                                if ((isset($_GET['minpricerange']) && $_GET['minpricerange'] == 30000) && (isset($_GET['maxpricerange']) && $_GET['maxpricerange'] == 40000)) {
                                                    $chk4 = "checked";
                                                } else {
                                                    $chk4 = "";
                                                }
                                                if ((isset($_GET['minpricerange']) && $_GET['minpricerange'] == 40000) && (isset($_GET['maxpricerange']) && $_GET['maxpricerange'] == 50000)) {
                                                    $chk5 = "checked";
                                                } else {
                                                    $chk5 = "";
                                                }
                                                if ((isset($_GET['maxpricerange']) && $_GET['maxpricerange'] > 50000)) {
                                                    $chk6 = "checked";
                                                } else {
                                                    $chk6 = "";
                                                }
                                                ?>
                                            <li class="brandname_class">
                                                <input type="checkbox" name="brandid" data-value="1" class="pricechk"
                                                    id="price_1" <?php echo $chk1; ?>>
                                                <label class="form-label fs-12 " for="price_1"> <span
                                                        class="label-check">0-10000 </span> </label>
                                            </li>
                                            <li class="brandname_class">
                                                <input type="checkbox" name="brandid" data-value="2" class="pricechk"
                                                    id="price_2" <?php echo $chk2; ?>>
                                                <label class="form-label fs-12 " for="price_2"><span
                                                        class="label-check"> 10000 - 20000</span> </label>
                                            </li>
                                            <li class="brandname_class">
                                                <input type="checkbox" name="brandid" data-value="3" class="pricechk"
                                                    id="price_3" <?php echo $chk3; ?>>
                                                <label class="form-label fs-12 " for="price_3"> <span
                                                        class="label-check">20000 - 30000</span> </label>
                                            </li>
                                            <?php // if (isset($categoryname) && $categoryname == "Home Lockers") { 
                                        ?>
                                            <li class="brandname_class">
                                                <input type="checkbox" name="brandid" data-value="4" class="pricechk"
                                                    id="price_4" <?php echo $chk4; ?>>
                                                <label class="form-label fs-12 " for="price_4"> <span
                                                        class="label-check">30000 - 40000</span> </label>
                                            </li>
                                            <li class="brandname_class">
                                                <input type="checkbox" name="brandid" data-value="5" class="pricechk"
                                                    id="price_5" <?php echo $chk5; ?>>
                                                <label class="form-label fs-12 " for="price_5"> <span
                                                        class="label-check">40000 - 50000</span> </label>
                                            </li>
                                            <li class="brandname_class">
                                                <input type="checkbox" name="brandid" data-value="6" class="pricechk"
                                                    id="price_6" <?php echo $chk6; ?>>
                                                <label class="form-label fs-12 " for="price_6"> <span
                                                        class="label-check">Above 50000</span> </label>
                                            </li>
                                            <?php //}  
                                        ?>
                                        </ul>
                                        <ul>
                                            <li>
                                                <div class="form-control price-range-form 0">
                                                    <!--<div class="row search pt-15  pl-10" >-->
                                                    <form action="/" method="post">
                                                        <div class=" text-center  grid-space-1 searchinputbx col-md-10">
                                                            <div class="priceinput">
                                                                <input type="hidden" name="hidden_minval_ref"
                                                                    id="hidden_minval_ref"
                                                                    value="<?php echo  isset($_GET['minpricerange']) && $_GET['minpricerange'] != "0" ? $_GET['minpricerange'] : 0 ?>" />
                                                                <input type="hidden" name="hidden_minval"
                                                                    id="hidden_minval"
                                                                    value="<?php echo  isset($_GET['minpricerange']) && $_GET['minpricerange'] != "0" ? $_GET['minpricerange'] : 0 ?>" />
                                                                <input type="text" name="minval" id="minval"
                                                                    class="input-lg range-input"
                                                                    value="<?php echo  isset($_GET['minpricerange']) && $_GET['minpricerange'] != "0" ? $_GET['minpricerange'] : 0 ?>" />
                                                            </div>
                                                            <div class=""> &nbsp;to&nbsp;
                                                            </div>
                                                            <div class="priceinput">
                                                                <input type="hidden" name="hidden_maxval"
                                                                    id="hidden_maxval"
                                                                    value="<?php echo  isset($_GET['maxpricerange']) && $_GET['maxpricerange'] != "0" ? $_GET['maxpricerange']  : $maxprice ?>" />
                                                                <input type="hidden" name="hidden_maxval_ref"
                                                                    id="hidden_maxval_ref"
                                                                    value="<?php echo  $maxprice ?>" />
                                                                <input type="text" name="maxval" id="maxval"
                                                                    class="input-lg range-input"
                                                                    value="<?php echo  isset($_GET['maxpricerange']) && $_GET['maxpricerange'] != "0" ? $_GET['maxpricerange'] : $maxprice ?>" />
                                                            </div>
                                                            <div class="middleBar">
                                                                <input type="submit" id="pricerange"
                                                                    class="btn btn-default range-btn" value="Go"
                                                                    style="margin-left:7px;">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="accordian-group">
                                    <div class="filter-title ff-sans">
                                        <h4 class="fs-18 ff-sans"> Category</h4>
                                        <span><i class="fa fa-plus"></i></span>
                                    </div>

                                    <div class="filter-list">
                                        <ul class="brandparent">
                                            
                                            @foreach($category as $cat)
                                            
                                            @if(isset($_GET['catid']) && $cat['cat_id'] == $_GET['catid'])
                                                $chk1 = "checked";
                                            @else
                                                <!-- $chk1 = ""; -->
                                            @endif

                                            <li class="brandname_class">
                                                <input type="checkbox" name="brandid" data-value="{{ $cat->cat_id}}"
                                                    class="categorychk" id="category-filter{{ $cat->cat_id}}"
                                                    {{ $chk1;}}>
                                                <label class="form-label fs-12" for="category-filter{{ $cat->cat_id }}">
                                                    <span class="label-check">{{ $cat->cat_name; }}</span>
                                                </label>
                                            </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>        
                </div>
                <!-- End Showing Product -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content" id="mdcontent">
                <span>
                    <i class="fa fa-check" style="color:#026ca8;font-size: 30px;margin: -10px 0px 0px -7px;"></i>
                </span>
                <p id="added"
                    style="display: none;text-align: center; margin: -24px 0 0px;color:#f2f2f2;font-size: 18px">Added to
                    your wishlist</p>
                <p id="removed"
                    style="display: none; text-align: center; margin: -24px 0 0px;color:#f2f2f2;font-size: 18px ;">
                    Removed from your wishlist</p>
            </div>
        </div>
    </div>
    <style>
    @keyframes glow {
        0% {
            box-shadow: 0 0 1px 1px rgba(255, 255, 255, 0.9);
        }

        20%,
        100% {
            box-shadow: 0 0 1px 1px rgba(255, 255, 255, 0.9), 0 0 3px 8px #026ca8, 0 0 2px 12px #FFF;
        }
    }

    /* Firefox */
    @-moz-keyframes glow {
        0% {
            box-shadow: 0 0 1px 1px rgba(255, 255, 255, 0.9);
        }

        20%,
        100% {
            box-shadow: 0 0 1px 1px rgba(255, 255, 255, 0.9), 0 0 3px 8px #026ca8, 0 0 2px 12px #FFF;
        }
    }

    /* Safari and Chrome */
    @-webkit-keyframes glow {
        0% {
            box-shadow: 0 0 1px 1px #FFF;
        }

        20%,
        100% {
            box-shadow: 0 0 1px 1px #FFF, 0 0 3px 8px #026ca8, 0 0 2px 12px #FFF;
        }
    }

    /* .feedback {
            background-color: #026ca8;
            color: white;
            padding: 10px 20px;
            border-radius: 4px;
            border: #026ca8;
        } */

    #mybutton {
        position: fixed;
        bottom: 20px;
        right: 10px;
        z-index: 2;
    }

    /* The Modal (background) */
    .modal {
        top: auto;
        bottom: 10px;
        outline: none;
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        /*z-index: 1;  Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        /*top: 0;*/
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: transparent !important;
        /*background-color: rgb(0,0,0);  Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #323232;
        margin: 32% auto;
        /*margin-top:32%*/
        padding: 20px;
        border: 1px solid #888;
        width: 26%;
    }

    /* The Close Button */
    .close {
        /*color: #aaaaaa;*/
        color: #000;
        float: left;
        font-size: 28px;
        font-weight: bold;
    }

    .modal-backdrop {
        opacity: none !important;
    }

    .ui-pnotify {
        top: 83% !important;
    }
    </style>
</div>
<!--end left area -->
@endsection