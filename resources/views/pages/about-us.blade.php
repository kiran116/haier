@extends('layouts.appmaster')
@section('content')

<section class="show-banner">
    <div class="banner-img">
        <!-- <img src="images/haier/banners/about-us.jpg" alt="about-us"> -->
        <img src="{{ asset('public/images/haier/banners/about-us.jpg')}}" class="img-fluid">
    </div>
</section>
<div class="about-us-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="about-para">
                    <div class="page-title">About Us </div>
                </div>
                <div class="about-us-details">
                    <div class="about-us-item">
                        <h3> Haier IN</h3>
                        <p>
                            Haier India is a 100 percent subsidiary of Haier Group, a fast-growing consumer durables company. Haier is the world’s No. 1 brand of Major Appliances* for 13 consecutive years, as per Euromonitor International. The company initiated its commercial operations in India in January 2004 and offers a wide range of products across categories like Refrigerators, Air conditioners, Washing Machines Wine Cellars, Deep Freezers, Water Heaters, LED TVs, and commercial Air Conditioners.
                        </p>
                        <p>
                            Haier is also the world’s no.1 brand in Refrigeration appliances, Home Laundry appliances, Freezer and Wine Chillers. Known for introducing innovative products in India and the world, Haier is best recognized for its revolutionary products such as Bottom Mounted (BMR) and French door Refrigerators with smart convertible sections, Washing Machines like innovations like Self Clean Technology (SCT) and Near Zero Pressure (NZP), Water Heaters with shockproof technology and many other customer-inspired innovations.
                        </p>
                    </div>
                    <div class="about-us-item">
                        <h3>HAIER INDIA MANUFACTURING</h3>
                        <p>
                            Every Haier product stems from its brand philosophy of 'Inspired Living' and is designed featuring the best-in-class innovation and technology keeping the day-to-day customer needs in mind. Haier India initiated the manufacturing of refrigerators in India in 2007 at its factory in Ranjangaon, Pune, which was further expanded into the company’s first industrial park in India. Inaugurated in November 2017, the industrial park has an annual production capacity of 1.8 million units of refrigerators and .5 million units each of other categories such as Washing Machines, Air Conditioners, LED TVs, and Water Heaters.
                        </p>
                        <p>
                            In March 2019, Haier conducted the groundbreaking ceremony of its second Industrial Park in Greater Noida, Uttar Pradesh. With this investment, Haier India aims to scale up the production capacity of categories like refrigerators, washing machines, air conditioners, LED panels, and water heaters. This second industrial Park of the company will hold a capacity to produce two million units of refrigerators, one million units of LED TVs, one million units of washing machines and air conditioners in a year when fully completed. With its second industrial park, Haier India is expected to generate around 4,000 new direct employment opportunities along with over 10,000 indirect jobs in the country. This will also serve as an opportunity for vendors and OEMs (original equipment manufacturers) to scale up and bring about an expanded business with increased local production and lesser dependence on imports.
                        </p>
                        <p>
                            The company also won the prestigious 'Make in India' Award for Excellence under the consumer durables category in the year 2015 for contribution to the growth of the country’s economy.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection