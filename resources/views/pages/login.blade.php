@extends('layouts.appmaster')
@section('content')
<div class="container">
     <div class="row about body-min-height">
         <div class="col-lg-4 col-lg-offset-4 personal-details">
             <div class="row">

                 <div class="login" id="">
                     <div class="about-para">
                         <div class="page-title">login</div>
                     </div>
                     <div class="col-md-12 col-xs-12" id="">
                         <div class="alert login-alert hidden">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                             <strong id="login_msg"> </strong>
                         </div>
                         <form action="{{route('logincheck')}}" class="sign-up" method="post" style="margin-top: 20px !important">
                         @csrf
                            <div class="row">
                                 <div class="col-md-12 form-group">

                                     <label for="login_phone" class="field prepend-icon" style="width: 100%;">
                                         <input type="text" name="cust_emailid"  class="form-control number" placeholder="Email Id" autocomplete="off">
                                     </label>
                                 </div>
                             </div>
                             <div class="row">
                                 <div class="col-md-12 form-group">
                                    <label for="login_password" class="field prepend-icon" style="width: 100%;">
                                         <input type="password" name="cust_password"  class="form-control" placeholder="Password" autocomplete="off">
                                    </label>
                                 </div>
                             </div>
                             <div class="row">
                                <div class="col-md-6 form-group reg-frm-grp">
                                    <!-- comment out by priya for local test-->
                                    <!-- <div class="g-recaptcha" data-sitekey="6LcSXwYeAAAAALpnwIx4e7XdCQqnPAg4qaljez3L"></div> -->
                                    <!--added by priya for QA site on 11-01--22-->
                                    <!-- <div class="g-recaptcha" data-sitekey="6LeZ3T4eAAAAAITw4_VnMT-5MSpvaJaRY33_KCkk"></div> -->
                                </div> 
                             <a href="javascript:;" class="hidden" id="resend" onclick="resend(this);" style="margin-top: 16px;margin-right: 7px; font-size: 20px">Resend OTP</a>
                            <div class="row otp hidden">
                                <div class="col-md-12 form-group reg-frm-grp">
                                    <label for="pwd" class="field prepend-icon">
                                         <input type="text" name="otp" id="otp" maxlength="6" class="form-control" placeholder="OTP">
                                         <label for="otp" class="field-icon"></label>
                                    </label>
                                </div>
                             </div>
                             <div class="row">
                                 <div class="col-md-12 form-group">
                                     <label class="container check-container loginlb">
                                         <a href="forgot_password.php">Forgot Password?</a>
                                     </label>
                                     <button type="submit" class="btn btn-plane ruby-color update-btn ">Submit</button>
                                     <label class="container check-container newuser">
                                         <a href="{{route('register')}}" class="user-registraion">Register Now</a>
                                     </label>
                                 </div>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
@endsection