<div class="header-overlay"></div>
<!-- Google Tag Manager -->

<!-- End Google Tag Manager -->
<div class="top-header">
    <div class="service-text">
        <a href="https://api.whatsapp.com/send/?phone=919930055200&text=hi" target="_blank"><b> <img
                    src="{{ asset('public/images/whatsapp.png')}}" alt="whatsapp-icon" class="whatsapp-img"></i>&nbsp;
                9930055200</a>&nbsp;&nbsp;
        <span>For service</span>
        <a href="tel:18004199999">1800-419-9999</a>
    </div>
</div>
<header>

    <div class="header-area">
        <div class="head-top-with-middle">

            <!--             <div class="header-top header-top-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="language-option ">
                                <nav>
                                    <ul class="hidden-xs fw-500">
                                  
                                        <li><a href="login"><i class="fa fa-mobile mobile-icons" aria-hidden="true"></i> &nbsp;Download App</a></li>  
                                        <li><a href="storelocator.php">Store Locator</a></li>
                                        <li id="loginid"><a href="javascript:void(0)">Customer Care</a></li>
                                        <li id="loginid"><a href="logout.php">Log Out</a></li>
                                 
                                        <li><a href="login"><i class="fa fa-mobile mobile-icons" aria-hidden="true"></i> &nbsp;Download App</a></li>  
                                        <li><a href="login">Store Locator</a></li>
                                        <li id="loginid"><a href="javascript:void(0)">Customer Care</a></li>
                                   
                                    </ul>                                   
                                </nav>
                            </div>                         
                        </div>
                    </div>
                </div>
            </div>-->
            <div class=" header-middle-toppadding">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="header-info top-header">
                                <div class="header-contact">
                                    <ul>
                                        <li><a href="mailto:in.eshop@haierindia.com" style="white-space: nowrap;"><i
                                                    class="fa fa-envelope-o" aria-hidden="true"></i>
                                                <span>in.eshop@haierindia.com</span></a></li>
                                        <!-- <li><a href="tel:18002095511"><i class="fa fa-phone" aria-hidden="true"></i> <span>1800 209 5511</span></a></li> -->
                                    </ul>
                                </div>
                                <div class="header-contact">
                                    <ul>
                                        <li><a href="about-us" class=" fs-11">About Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="main-header">
                                <div class="logo-wrap">
                                    <div class="logo">
                                        <!-- <a href="javascript:void(0)">
                                            <i class="fa fa-bars"></i>
                                        </a> -->
                                        <!-- <a href="">
                                        <img src="{{ asset('public/images/haier-logo.png')}}" alt="haier-appliances"
                                            class="img-responsive logo">
                                        </a> -->
                                    </div>
                                </div>

                                <div class="header-info">
                                    <div class="search-wrap">
                                        <div class="search search-form" id="search-field">
                                            <form id="searchForm123" method="post" class="middleBar ">
                                                <div class="searchinputbx">
                                                    <input type="search" placeholder="Search For Product"
                                                        name="searchInput" id="searchInput"
                                                        class="form-control input-lg search" required>
                                                    <div class="input-search">
                                                        <!-- <input type="submit" class="btn btn-default btn-block btn-lg" value="Search"> -->
                                                        <button type="submit" onclick="fbq('track', 'Search');">
                                                            <img src="{{ asset('public/images/haier/icons/loupe.png')}}"
                                                                alt="">
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>
                                            <div class="search-btn open">
                                                <img src="{{ asset('public/images/haier/icons/cancel.png')}}"
                                                    class="cancel" alt="">
                                                <img src="{{ asset('public/images/haier/icons/loupe.png')}}"
                                                    class="search" alt="">
                                                <span>search</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section-icon">
                                        <div class="menu-with-card middleBar">
                                            <div class="row  header-items myaccount-list">
                                                <!-- <div class="header-item cart3" id="searchProduct">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </div> -->
                                                <div class="header-item menu main-menu home-one-menu">

                                                    <div class="inner-menu-btn" data-toggle="tooltip"
                                                        data-placement="top" title="" data-original-title="Login">
                                                        <i class="fa fa-user-o header-icons"></i>
                                                        <nav class="profile-menu-panel" class="pl-0">
                                                            <ul class="megamenu new_menu first-mega1 myaccount">
                                                                <li>
                                                                    <ul class="single-mega-1">
                                                                        <li>
                                                                            <ul class="list-unstyled">
                                                                                <li><a href="my-order">My order</a></li>
                                                                                <li><a href="my-wishlist">My
                                                                                        wishlist</a></li>
                                                                                <!-- <li><a href="deals-for-you.php">Deal for you</a></li> -->
                                                                                <li><a href="personal_details">Personal
                                                                                        Details</a></li>
                                                                                <li><a href="change_password">Change
                                                                                        Password</a></li>
                                                                                <li><a
                                                                                        href="subscription_details">Subscriptions</a>
                                                                                </li>
                                                                                <li><a href="logout.php">Logout</a></li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div>


                                                    <a id="loginid" href="login" data-toggle="tooltip"
                                                        data-placement="top" title="" data-original-title="Login">
                                                        <i class="fa fa-user-o header-icons"></i>
                                                    </a>
                                                    <!-- <a id="loginid" href="login"> <span>Login</span></a> -->

                                                </div>
                                                <!-- <div class="header-item">
                                                    <a href=" data-toggle="tooltip" data-placement="top" title="" data-original-title="Wishlist">
                                                        <i class="fa fa-heart-o header-icons"></i>
                                                    </a>
                                                </div> -->
                                                <div class="header-item cart-item cart3">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-menu-area hidden-xs hidden-sm">
                <div class="navbar-scrollup container containermenu">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="menu main-menu home-one-menu">
                                <nav>
                                    <ul>
                                        
                                       
                                        <!--here remove foreach loop -->


                                        <li>
                                            <a href="{{route('special-offer')}}">Special Offers</a>
                                        </li>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Start Mobile Menu -->

            <!-- End Mobile Menu -->
            <style>
            .containermenu {
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }
            </style>

            <!-- End Main Menu -->
        </div>
</header>