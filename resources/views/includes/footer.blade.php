<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/brands.min.css">

<footer class="hidden-xs hidden-sm">
     <div class="footer-area">
         <div class="scroll-top">
             <a href="#">
                 <!-- <img src="images/haier/icons/up-arrow-w.png" alt=""> -->
                 <img src="{{ asset('public/images/haier/icons/up-arrow-w.png')}}" alt="">
             </a>
         </div>
         <div class="footer-middle">
             <div class="container">
                 <div class="row">
                     <div class="col-md-2 col-sm-4 col-xs-12 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Contact Details</h4>
                             <div class="footer-top-list">
                                 <nav>
                                     <ul>
                                         <li><a href="{{route('about-us')}}">About us</a></li>
                                         <li><a href="contact-us">Contact Us</a></li>
                                     </ul>
                                 </nav>
                             </div>
                             <div class="social-links">
                                 <div class="footer-address footer-enq">
                                     <p><b>For Sales Enquiries:</b><br>
                                         <a href="tel:+91-9930055400"><b> <i class="fa fa-phone"></i> +91-9930055400</b></a>
                                         <a href="maillto:in.eshop@haierindia.com"><b> <i class="fa fa-envelope"></i>in.eshop@haierindia.com</b></a>
                                     </p>
                                     <p><b>For Order Enquiries:</b><br>
                                         <a href="tel:+91-9930055800" target="_blank"><b> <i class="fa fa-phone"></i> +91-9930055800</b></a>
                                         <a href="maillto:customercare@haierindia.com"><b> <i class="fa fa-envelope"></i>in.eshop@haierindia.com</b></a>
                                     </p>
                                     <!-- <p><b>For After-Sales Service:</b><br>
                                         <a href="https://wa.me/8553049999" target="_blank"><b> <i class="fa fa-whatsapp"></i> 8553049999</b></a>
                                         <a href="tel:1800-419-9999"><b class="toll_free_no"> <i class="fa fa-phone"></i> 1800-419-9999 </b></a>
                                         <a href="tel:1800-419-9999"><b class="toll_free_no"> <i class="fa fa-phone"></i> 1800-102-9999</b></a>
                                         <a href="maillto:customercare@haierindia.com"><b> <i class="fa fa-envelope"></i>customercare@haierindia.com</b></a>
                                     </p> -->
                                     <p>Working hours: 10:30 am to 5:30 pm <br> Closed on Sundays and Public Holidays</p>
                                 </div>
                             </div>

                         </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-12 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Support</h4>
                             <div class="footer-top-list">
                                 <nav>
                                     <ul>
                                         <li><a href="terms-and-condition">Terms and Condition</a></li>
                                         <li><a href="privacy-policy">Privacy Policy</a></li>
                                     </ul>
                                 </nav>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-12 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Product Categories</h4>
                             <div class="footer-top-list">
                                 <nav>
                                     <ul>
                                         @foreach($cats as $cat)
                                            <li>{{$cat->cat_name}}</li>
                                         @endforeach
                                     </ul>
                                 </nav>
                             </div>
                         </div>
                     </div>

                     <div class="col-md-2 col-sm-4 col-xs-12 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Login</h4>
                             <div class="footer-top-list">
                                 <nav>
                                     <ul>
                                         <?php
                                            if (isset($_SESSION['gsslogin_user'])) {

                                            ?>
                                             <!--<li><a href="myaccount.php">My Account</a></li>-->
                                             <li><a href="my-order">My Orders</a></li>
                                             <li><a href="my-wishlist">Wish List</a></li>
                                             <!-- <li><a href="deals-for-you.php">Deals for you</a></li> -->
                                             <li><a href="shopping-cart">Shopping Cart</a></li>
                                             <li><a>&nbsp;</a></li>
                                         <?php } else { ?>
                                             <!--<li><a href="login">My Account</a></li>-->
                                             <li><a href="{{route('my-order')}}">My Orders</a></li>
                                             <li><a href="login?backurl=my-wishlist">Wish List</a></li>
                                             <!-- <li><a href="login">Deals for you</a></li> -->
                                             <li><a href="login?backurl=shopping-cart">Shopping Cart</a></li>
                                             <li><a>&nbsp;</a></li>
                                         <?php } ?>
                                     </ul>
                                 </nav>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-4 col-sm-12 col-xs-12 footer-lists">
                         <h4 class="footer-title">Registered Address</h4>
                         <div class="social-links">
                             <div class="footer-address">
                                 <p>Haier Appliances India Pvt Ltd</p>
                                 <p>Building Number 1, Okhla Industrial Estate, Phase III,</p>
                                 <p>Opposite Modi Mill, New Delhi-110020.</p>
                             </div>
                         </div>
                         <div class="social-links mt-20">
                             <nav>
                                 <ul>
                                     <li><a href="https://www.facebook.com/HaierIndia/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                     <li><a href="https://twitter.com/IndiaHaier" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                     <li><a href="https://www.youtube.com/c/HaierIndia" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                     <li><a href="https://www.instagram.com/haierindia" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                     <li><a href="https://linkedin.com/company/haier" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                     <li><a href="https://wa.me/918553049999?" target="_blank"><i class="fa fa-whatsapp"></i></a></li>

                                 </ul>
                             </nav>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </footer>
 <footer class="visible-xs visible-sm">
     <div class="footer-area">
         <div class="scroll-top">
             <a href="#">
                 <img src="images/haier/icons/up-arrow-w.png" alt="">
             </a>
         </div>
         <div class="footer-middle">
             <div class="container">
                 <div class="row">
                     <div class="col-md-2 col-sm-4 col-xs-6 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Contact Details</h4>
                             <div class="footer-top-list">
                                 <nav>
                                     <ul>
                                         <li><a href="{{route('about-us')}}">About us</a></li>
                                         <li><a href="{{route('contact-us')}}">Contact Us</a></li>
                                     </ul>
                                 </nav>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-6 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Support</h4>
                             <div class="footer-top-list">
                                 <nav>
                                     <ul>
                                         <li><a href="terms-and-condition">Terms and Condition</a></li>
                                         <li><a href="privacy-policy">Privacy Policy</a></li>
                                     </ul>
                                 </nav>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-2 col-sm-4 col-xs-6 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Product Categories</h4>
                             <div class="footer-top-list">
                                 <nav>
                                     <ul>
                                        @foreach($cats as $cat)
                                            <li>{{$cat->cat_name}}</li>
                                        @endforeach
                                     </ul>
                                 </nav>
                             </div>
                         </div>
                     </div>

                     <div class="col-md-2 col-sm-4 col-xs-6 footer-lists">
                         <div class="footer-question">
                             <h4 class="footer-title">Login</h4>
                             <div class="footer-top-list">
                                 <nav>
                                    <ul>
                                        
                                             <!--<li><a href="myaccount.php">My Account</a></li>-->
                                             <li><a href="{{ route('my-order') }}">My Orders</a></li>
                                             <li><a href="{{ route('my-wishlist')}}">Wish List</a></li>
                                             <!-- <li><a href="deals-for-you.php">Deals for you</a></li> -->
                                             <li><a href="{{route('shopping-cart')}}">Shopping Cart</a></li>
                                             <li><a>&nbsp;</a></li>
                                             <!--<li><a href="login">My Account</a></li>-->
                                             <!-- <li><a href="login?backurl=my-order">My Orders</a></li>
                                             <li><a href="login?backurl=my-wishlist">Wish List</a></li> -->
                                             <!-- <li><a href="login">Deals for you</a></li> -->
                                             <!-- <li><a href="login?backurl=shopping-cart">Shopping Cart</a></li>
                                             <li><a>&nbsp;</a></li> -->
                                    </ul>
                                 </nav>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-4 col-sm-12 col-xs-12 footer-lists">
                         <h4 class="footer-title">Registered Address</h4>
                         <div class="social-links">
                             <div class="footer-address">
                                 <p>Haier Appliances India Pvt Ltd</p>
                                 <p>Building Number 1, Okhla</p>
                                 <p>Industrial Estate, Phase III, Opposite Modi Mill, New Delhi-110020.</p>
                             </div>
                         </div>
                         <div class="social-links">
                             <div class="footer-address footer-enq">
                                 <p><b>For Sales Enquiries:</b>
                                     <a href="tel:+91-9930055400"><b> <i class="fa fa-phone"></i> +91-9930055400</b></a>
                                     <a href="maillto:in.eshop@haierindia.com"><b> <i class="fa fa-envelope"></i>in.eshop@haierindia.com</b></a>
                                 </p>
                                 <p><b>For Order Enquiries:</b>
                                     <a href="tel:+91-9930055800" target="_blank"><b> <i class="fa fa-phone"></i> +91-9930055800</b></a>
                                     <a href="maillto:customercare@haierindia.com"><b> <i class="fa fa-envelope"></i>in.eshop@haierindia.com</b></a>
                                 </p>
                                 <p><b>For After-Sales Service:</b>
                                     <a href="https://wa.me/8553049999" target="_blank"><b> <i class="fa fa-whatsapp"></i> 8553049999</b></a>
                                     <a href="tel:1800-419-9999"><b class="toll_free_no"> <i class="fa fa-phone"></i> 1800-419-9999 / 1800-102-9999</b></a>
                                     <a href="maillto:customercare@haierindia.com"><b> <i class="fa fa-envelope"></i>customercare@haierindia.com</b></a>
                                 </p>
                                 <p>Working hours: 10:30 am to 5:30 pm <br> Closed on Sundays and Public Holidays</p>
                             </div>
                         </div>
                         <div class="social-links mt-20">
                             <nav>
                                 <ul>
                                     <li><a href="https://www.facebook.com/HaierIndia/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                     <li><a href="https://twitter.com/IndiaHaier" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                     <li><a href="https://www.youtube.com/c/HaierIndia" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                     <li><a href="https://www.instagram.com/haierindia" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                     <li><a href="https://linkedin.com/company/haier" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                     <li><a href="https://wa.me/918553049999?" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                                 </ul>
                             </nav>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </footer>
 <div class="copy-right">
     <p>Copyright © 2021 <a href="https://shop.haierindia.com/">shop.haierindia.com</a> All Rights Reserved</p>
     <p>Site hosted and managed by Brandbuddiez technologies Pvt. Ltd.</p>
 </div>
 <!-- End Footer -->
<script>
     // $(document).on('click', '.camera_target_content .cameraContents .cameraContent', function(){
     //     var url=($($('.camera_src .data-img-holder')[$('.camera_target_content .cameraContents .cameraContent').index($(this))]).data('itemlink'));
     //     window.open(url, '_blank');
     // })

     //script for url pages add on 07/06/19
    var backurl = getParameterByName('backurl');
    function getParameterByName(name, url) {
         if (!url) url = window.location.href;
         name = name.replace(/[\[\]]/g, '\\$&');
         var regex = new RegExp('[?&]' + name + '(=([^#]*)|#|$)'),
             results = regex.exec(url);
         if (!results) return null;
         if (!results[2]) return '';
         return decodeURIComponent(results[2].replace(/\+/g, ' '));
     }

 </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- <script src="{{asset ('public/js/jquery-1.12.4.min.js')}}"></script> -->
<script src="{{asset ('public/js/aos.js')}}"></script>
<script src="{{asset ('public/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset ('public/js/bootstrap.min.js')}}"></script>
<script src="{{asset ('public/js/camera.min-org.js')}}"></script>
<script src="{{asset ('public/js/camera.min.js')}}"></script>

<script src="{{asset ('public/js/jquery-confirm.min.js')}}"></script>
<script src="{{asset ('public/js/jquery-ui.js')}}"></script>
<script src="{{asset ('public/js/jquery-easing.1.3.js')}}"></script>
<script src="{{asset ('public/js/jquery-meanmenu.js')}}"></script>
<script src="{{asset ('public/js/jquery-time-to.min.js')}}"></script>
<script src="{{asset ('public/js/owl.carousel.min.js')}}"></script>
<script src="{{asset ('public/js/main.js')}}"></script>
<script type="text/javascript" src="{{ asset ('public/js/datepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
