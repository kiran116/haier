<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\C_category;
use App\Models\C_banner;
use App\Models\C_product_image;
use App\Models\C_product_type;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        // $cat = C_category::find(1)->product;
        // dd($cat);exit;
        $cats = C_category::all();
        $home_banner = DB::select('select c_bnr_type_id, c_bnr_redirect_link, c_bnr_img from c_banner where c_bnr_type_id in(1,8) and c_bnr_status="Activated" order by c_bnr_priority asc');
        // dd($home_banner);
        // $banners = C_banner::where('c_bnr_status','Activated')->where('c_bnr_type_id',1)->get();
        // dd($banners);
        // $products = C_product_image::where("c_prd_img_status","Activated")->where('c_prd_img_priority',1)->limit(8)->get();
        // dd($products);exit;
        $c_product_types = C_product_type::where('c_pt_status',"Activated")->limit(2)->get();
        // dd($c_product_types);
                
                $flag_product = ('SELECT * from c_product
                LEFT JOIN c_product_type on c_product_type.c_pt_id = c_product.c_prd_product_type_id
                LEFT JOIN (SELECT t1.* FROM c_pricing t1 WHERE t1.c_price_id  = (SELECT t2.c_price_id  FROM c_pricing t2  WHERE 
                t2.c_price_prd_id = t1.c_price_prd_id and  t2.c_price_effectivedate <= curdate() and t2.c_price_status="Activated" 
                order by t2.c_price_effectivedate desc, t2.c_price_id desc limit 1  ) 
                ) price_max on price_max.c_price_prd_id = c_product.c_prd_id  
                left join c_brand_master on c_brand_master.brand_id = c_product.c_prd_brand_id
                left join c_category on c_category.cat_id = c_product_type.c_pt_cat_id
                LEFT JOIN c_wishlist on c_wishlist.c_wish_prd_id = c_product.c_prd_id
                LEFT JOIN c_product_image on c_prd_img_name !="" and c_prd_img_status="Activated" and c_prd_img_priority = 1 and c_prd_img_prd_id = c_product.c_prd_id
                LEFT JOIN (select * from c_offer WHERE (curdate() BETWEEN c_off_validity_start_date and c_off_validity_end_date) AND c_off_cmp_id = 10 and c_off_status="Activated") as offer on offer.c_off_prd_id = c_product.c_prd_id
                WHERE   c_product.c_prd_status = "Activated" and c_product.c_prd_status = "Activated" and c_product.c_prd_flagship_status = "on" ORDER BY `c_product`.`c_flgship_prd_priority` ASC');
                
                $result = $flag_product;
               
                // dd($result);
    // $flagship_products = $result;
    $flagship_products = DB::select($flag_product);
    // dd($flagship_products);
    
    $newly_arrival_products = DB::select('SELECT * from c_product
    LEFT JOIN c_product_type on c_product_type.c_pt_id = c_product.c_prd_product_type_id
    LEFT JOIN (SELECT t1.* FROM c_pricing t1 WHERE t1.c_price_id  = (SELECT t2.c_price_id  FROM c_pricing t2  WHERE 
    t2.c_price_prd_id = t1.c_price_prd_id and  t2.c_price_effectivedate <= curdate() and t2.c_price_status="Activated" 
    order by t2.c_price_effectivedate desc, t2.c_price_id desc limit 1  ) 
    ) price_max on price_max.c_price_prd_id = c_product.c_prd_id  
    left join c_brand_master on c_brand_master.brand_id = c_product.c_prd_brand_id
    left join c_category on c_category.cat_id = c_product_type.c_pt_cat_id
    LEFT JOIN c_wishlist on c_wishlist.c_wish_prd_id = c_product.c_prd_id
    LEFT JOIN c_product_image on c_prd_img_name !="" and c_prd_img_status="Activated" and c_prd_img_priority = 1 and c_prd_img_prd_id = c_product.c_prd_id
    LEFT JOIN (select * from c_offer WHERE (curdate() BETWEEN c_off_validity_start_date and c_off_validity_end_date) AND c_off_cmp_id = 10 and c_off_status="Activated") as offer on offer.c_off_prd_id = c_product.c_prd_id
    WHERE   c_product.c_prd_status = "Activated" and c_product.c_prd_status = "Activated" and c_product.c_prd_new_status = "on" ORDER BY `c_product`.`c_new_prd_priority` ASC');
    // dd($newly_arrival_products);
        

        $product_features = DB::table('c_product_feature')
                                ->where('c_prdfeat_status','=',"Activated")
                                ->where('c_prdfeat_name','<>'," ")
                                ->orderBy('c_prdfeat_priority', 'ASC')
                                ->limit(3)
                                ->get(); 

        $best_sellers = DB::table('c_product')
                            ->join('c_product_type','c_product_type.c_pt_id', '=', 'c_product.c_prd_product_type_id')
                            ->join('c_brand_master','c_brand_master.brand_id','=','c_product.c_prd_brand_id')
                            ->join('c_category','c_category.cat_id', '=', 'c_product_type.c_pt_cat_id')
                            ->join('c_wishlist','c_wishlist.c_wish_prd_id', '=' ,'c_product.c_prd_id')
                            ->join('c_product_image','c_prd_img_prd_id', '=', 'c_product.c_prd_id')
                            ->where('c_product_image.c_prd_img_status','=',"Activated")
                            ->where('c_product_image.c_prd_img_priority', '=',1)
                            ->where('c_product.c_prd_status', '=', "Activated")
                            ->where('c_product.c_best_sell_product','=','on')
                            ->orderBy('c_product.c_bestsell_prd_priority','ASC')
                            ->get();
        // dd($best_sellers);exit;

        $special_offers =DB::select('SELECT * from c_product
        LEFT JOIN c_product_type on c_product_type.c_pt_id = c_product.c_prd_product_type_id
        LEFT JOIN (SELECT t1.* FROM c_pricing t1 WHERE t1.c_price_id  = (SELECT t2.c_price_id  FROM c_pricing t2  WHERE 
        t2.c_price_prd_id = t1.c_price_prd_id and  t2.c_price_effectivedate <= curdate() and t2.c_price_status="Activated" 
        order by t2.c_price_effectivedate desc, t2.c_price_id desc limit 1  ) 
        ) price_max on price_max.c_price_prd_id = c_product.c_prd_id  
        left join c_brand_master on c_brand_master.brand_id = c_product.c_prd_brand_id
        left join c_category on c_category.cat_id = c_product_type.c_pt_cat_id
        LEFT JOIN c_wishlist on c_wishlist.c_wish_prd_id = c_product.c_prd_id
        LEFT JOIN c_product_image on c_prd_img_name !="" and c_prd_img_status="Activated" and c_prd_img_priority = 1 and c_prd_img_prd_id = c_product.c_prd_id
        LEFT JOIN (select * from c_offer WHERE (curdate() BETWEEN c_off_validity_start_date and c_off_validity_end_date) AND c_off_cmp_id = 10 and c_off_status="Activated") as offer on offer.c_off_prd_id = c_product.c_prd_id
        WHERE   c_product.c_prd_status = "Activated" and c_product.c_prd_status = "Activated" and c_product.c_prd_special_offer = "on" ORDER BY `c_product`.`c_special_prd_priority` ASC');
        // dd($special_offers);
        
        
        $category = DB::table('c_category')
                        ->where('c_category.cat_status','=',"Activated")
                        ->where('c_category.cat_key','=',0)
                        ->orderBy('c_category.cat_priority','ASC')
                        ->get();
        // dd($category);exit;
        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
                            ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
                            ->where('c_product_type.c_pt_status','=',"Activated")
                            ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
                            // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
                            ->orderBy('c_product_type.c_pt_priority','ASC')
                            ->get();
        // dd($header_product_cats);
     
        return view('pages.index',compact('cats','home_banner','c_product_types','flagship_products','newly_arrival_products','product_features','best_sellers','special_offers','category','header_product_cats'));
        // return view('pages.index');
    }
    public function about()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        // ->join('c_product_type','c_product_type.c_pt_cat_id','=','c_category.cat_id')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        // ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_category.cat_priority','ASC')
        // ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        // dd($category);exit;
        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
            ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
            ->where('c_product_type.c_pt_status','=',"Activated")
            ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
            // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
            ->orderBy('c_product_type.c_pt_priority','ASC')
            ->get();
        return view('pages.about-us',compact('category','header_product_cats','cats'));
    }
    public function contact()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;
        $header_product_cats = DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
            ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
            ->where('c_product_type.c_pt_status','=',"Activated")
            ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
            ->orderBy('c_product_type.c_pt_priority','ASC')
            ->get();
        return view('pages.contact-us',compact('cats','category','header_product_cats'));
    }
    public function termsandcondition()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
// dd($category);exit;
        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
            ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
            ->where('c_product_type.c_pt_status','=',"Activated")
            ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
            // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
            ->orderBy('c_product_type.c_pt_priority','ASC')
            ->get();
        return view('pages.terms-and-condition',compact('cats','category','header_product_cats'));
    }
    
    public function privacypolicy()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();

        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
            ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
            ->where('c_product_type.c_pt_status','=',"Activated")
            ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
            ->orderBy('c_product_type.c_pt_priority','ASC')
            ->get();
        return view('pages.privacy-policy',compact('category','header_product_cats','cats'));
    }
}