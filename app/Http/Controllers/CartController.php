<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\C_category;
use Illuminate\Support\Facades\DB;
class CartController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;

        $header_product_cats = DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        return view('pages.shopping-cart',compact('cats','category','header_product_cats'));
    }
    
    public function addToCart(Request $req)
    {
        $cats = C_category::all();   
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;

        $header_product_cats = DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();

        // $cart  = new Cart();
        return view('pages.shopping-cart',compact('cats','category','header_product_cats'));
    }
}