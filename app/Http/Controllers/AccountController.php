<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\C_category;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Auth;
class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home()
    {
        return view('home');
    }

    public function myorder()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        // ->join('c_product_type','c_product_type.c_pt_cat_id','=','c_category.cat_id')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        // ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_category.cat_priority','ASC')
        // ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        // dd($category);exit;
        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
            ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
            ->where('c_product_type.c_pt_status','=',"Activated")
            ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
            // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
            ->orderBy('c_product_type.c_pt_priority','ASC')
            ->get();
        return view('pages.my-order',compact('category','header_product_cats','cats'));
    }
    
    public function wishlist()
    {
        $cats = C_category::all();

        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;

        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        return view('pages.my-wishlist',compact('cats','category','header_product_cats'));
    }
    
    public function personaldetails()
    {
        $cats = C_category::all();

        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;
        
        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        $country = DB::table('country')->where('country_status','=',"Activated")->get();
        // dd($country);
        $state = DB::table('state')->where('state_status','=',"Activated")->get();
        // dd($state);
        return view('pages.personal_details',compact('cats','category','header_product_cats','country'));
    }

    public function changepassword()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;

        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        return view('pages.change-password',compact('cats','category','header_product_cats'));
    }

    public function subscriptiondetails()
    {
        $cats = C_category::all();
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;

        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        return view('pages.subscription-details',compact('cats','category','header_product_cats'));
    }

   
}
