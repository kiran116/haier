<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\C_category;
use App\Models\Product;
use App\Models\C_banner;
use App\Models\C_product_image;
use App\Models\C_product_type;
use Barryvdh\DomPDF\Facade\PDF;
class ProductController extends Controller
{
    //
    public function product_details($c_pt_name,$c_disp_name,$id)
    {   
        // $data = Product::find();
        // dd($data);
        $cats = C_category::all();   
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;

        $header_product_cats = DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        // dd($header_product_cats);

        $product = DB::table('c_product')
                ->join('c_product_type','c_product_type.c_pt_id', '=', 'c_product.c_prd_product_type_id')
                ->join('c_category','c_category.cat_id','=','c_product_type.c_pt_cat_id')
                ->join('c_pricing','c_pricing.c_price_prd_id','=','c_product.c_prd_id')
                ->join('c_product_image','c_product.c_prd_id','=','c_product_image.c_prd_img_prd_id')
                ->where('c_product.c_prd_id','=',$id,'and','c_product_image.c_prd_img_status',"Activated",'and','c_product_image.c_prd_img_prd_id','=',$id)
                ->orderBy('c_product_image.c_prd_img_priority','asc')
                ->first();
        // dd($product);

        $keyfeatures = DB::table('c_product_feature')
                    ->where('c_prdfeat_prdid','=', $id,'and','c_prdfeat_status','=',"Activated",'and','c_prdfeat_name','<>'," ")
                    ->orderBy('c_prdfeat_priority','asc')
                    ->limit(4)
                    ->get();
        // dd($keyfeatures);

        $specifications = DB::table('c_product_type_specific_category')
                    ->join('c_product_type','c_product_type.c_pt_id','=','c_product_type_specific_category.c_spf_product_type_id')
                    ->join('c_product','c_product.c_prd_product_type_id','c_product_type.c_pt_id')
                    ->where('c_product.c_prd_id','=',$id,'and','c_product_type_specific_category.c_spf_status','=',"Activated")
                    ->orderBy('c_product_type_specific_category.c_spf_priority','asc')   
                    ->get();
        // dd($specifications);

        // $data = DB::table('c_product_type')->get();
        // dd($data);

        $specifications_dynamic_cols = DB::table('c_dynamiccolumn')
                    ->where('c_dyn_status','=',"Activated")
                    ->orderBy('c_dyn_priority','asc')
                    ->get();
        // dd($specifications_dynamic_cols);

        // $specs = DB::table('table_1')->get();
        // dd($specs);

        $user_manuals = DB::table('c_product_usermanual')
                        // ->where('c_prd_usermanual_prd_id',$id)
                        ->get();
        // dd($user_manual);

        return view('pages.product_details',compact('category','header_product_cats','cats','product','keyfeatures','specifications','specifications_dynamic_cols','user_manuals'));       
    }

    public function loadpdf()
    {
        // $data = [
        //     'title' => "pdf title here",
        //     'date'  => date('m/d/y')
        // ];
            
        $user_manuals = DB::table('c_product_usermanual')
                ->where('c_prd_usermanual_id','=',"Activated")
                ->get();
        // dd($user_manual);
        
        $pdf = PDF::loadView('pages.testpdf',$user_manuals);
        // dd($pdf);exit;
        return $pdf->download('testpdf.pdf');   
    }
   
}