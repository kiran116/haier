<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class HeaderController extends Controller
{
    //
//     public function header()
//     {
//         $category = DB::table('c_category')
//         // ->join('c_product_type','c_product_type.c_pt_cat_id','=','c_category.cat_id')
//         ->where('c_category.cat_status','=',"Activated")
//         ->where('c_category.cat_key','=',0)
//         // ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
//         ->orderBy('c_category.cat_priority','ASC')
//         // ->orderBy('c_product_type.c_pt_priority','ASC')
//         ->get();
// // dd($category);exit;
//         $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
//             ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
//             ->where('c_product_type.c_pt_status','=',"Activated")
//             ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
//             // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
//             ->orderBy('c_product_type.c_pt_priority','ASC')
//             ->get();
//         return view('includes.header',compact('category','header_product_cats'));
//     }
    public function about()
    {
        return view('pages.about-us');
    }
    public function contact()
    {
        return view('pages.contact-us');
    }
    public function termsandcondition()
    {
        return view('pages.terms-and-condition');
    }
    public function privacypolicy()
    {
        return view('pages.privacy-policy');
    }
}
