<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\C_category;
use Illuminate\Support\Facades\DB;
class SpecialofferController extends Controller
{
    //
    public function specialoffer()
    {
        $num_rec_per_page = 24;
            if (isset($_GET["page"])) {
            $page = $_GET["page"];
            } 
            else 
            {
                $page = 1;
            };
        $start_from = ($page - 1) * $num_rec_per_page;
        $productdataarray = array();
        $productdataarray_left = array();
        $pricearray = array();
        $categoryname = "";
        $colarray = array();
        $colcount = 0;
        $productdataarray_left = array();
        $productTypearray = array();

        $cats = C_category::all();
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;
            
        $header_product_cats =DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
        ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
        ->where('c_product_type.c_pt_status','=',"Activated")
        ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
        ->orderBy('c_product_type.c_pt_priority','ASC')
        ->get();
        // dd($special_offers);

                $sql_query = ('SELECT *
                from c_product
                LEFT JOIN c_product_type on c_product_type.c_pt_id = c_product.c_prd_product_type_id
                LEFT JOIN c_category on c_category.cat_id = c_product_type.c_pt_cat_id 
                LEFT JOIN c_brand_master on c_brand_master.brand_id = c_product.c_prd_brand_id
                LEFT JOIN (SELECT t1.* FROM c_pricing t1 WHERE t1.c_price_id  = (SELECT t2.c_price_id  FROM c_pricing t2  WHERE 
                            t2.c_price_prd_id = t1.c_price_prd_id and
                            t2.c_price_effectivedate <= curdate()  and t2.c_price_status="Activated" order by t2.c_price_effectivedate desc, t2.c_price_id desc limit 1  ) ) price_max on price_max.c_price_prd_id = c_product.c_prd_id
                LEFT JOIN (SELECT t1.* FROM c_product_image t1 WHERE t1.c_prd_img_id  = (SELECT t2.c_prd_img_id  FROM c_product_image t2  WHERE
                            t2.c_prd_img_prd_id = t1.c_prd_img_prd_id and
                            c_prd_img_status="Activated" and c_prd_img_priority= 1  ) )
                price_max1 on price_max1.c_prd_img_prd_id = c_product.c_prd_id
                LEFT JOIN c_color on c_color.c_color_id = c_product.c_prd_color_id');
               
                $sql_product = DB::select($sql_query);
                // dd($sql_product);
                // $sql_product = DB::table('c_product')->where('c_product.c_prd_status', '=', "Activated",'and','c_prd_special_offer','=',"on")->get();
                // dd($sql_product);
                // $sql_product = DB::select('WHERE  c_product.c_prd_status = "Activated" and c_product.c_prd_special_offer = "on"');
                // dd($sql_product);
                // $sql_product = DB::table('c_product')->select('c_prd_status','c_prd_special_offer')->where('c_prd_status','=',"Activated",'and','c_prd_special_offer','=',"on");
                // dd($sql_product);
                
                   if (isset($_GET['catid'])) 
                    {
                         $sql_product = $sql_product . ' and c_category.cat_id = '.$_GET['catid']; //we get array of $_GET so we convert inot string    
                    }
                    
                    if (isset($_GET['minpricerange']) && isset($_GET['maxpricerange'])) {
                        $sql_product = $sql_product . ' and price_max.c_price_tot BETWEEN "' . $_GET['minpricerange'] . '" and "' . $_GET['maxpricerange'] . '"   ';
                               $sql_product = $sql_product . ' and price_max.c_price_mrp BETWEEN "'.$_GET['minpricerange'].'" and "'.$_GET['maxpricerange'].'"   ';
                    }

                    if (isset($_GET['colorid'])) {
                        $sql_product = $sql_product . ' and c_product.c_prd_color_id IN (' . implode(',', $_GET['colorid']) . ')'; //we get array of $_GET so we convert inot string    
                    }
                    // dd($sql_product);
                    $productcountfilter = count($sql_product);
                    // dd($productcountfilter);               

                    // $sql_product = $sql_product . 'ORDER BY CAST(`c_product`.`c_prd_currentstock` AS UNSIGNED) desc, `c_product`.`c_product_priority` ASC  LIMIT ' . $start_from . ', ' . $num_rec_per_page . ' ';
                    
                    $result_product = $sql_product;
                    foreach($result_product as $productdata)
                    {
                        $productdataarray[] = $productdata;
                        $pricearray[] = $productdata->c_price_tot; 
                    } 
                    
                    $maxprice = $minprice = 0;
                    $maxprice = !empty($pricearray) && max($pricearray) != "" ? max($pricearray) : 0;
                    $minprice = !empty($pricearray) && min($pricearray) != "" ? min($pricearray) : 0;

                    $product_left = 'SELECT * from c_product
                    LEFT JOIN c_product_type on c_product_type.c_pt_id = c_product.c_prd_product_type_id
                    LEFT JOIN c_category on c_category.cat_id = c_product_type.c_pt_cat_id 
                    LEFT JOIN c_brand_master on c_brand_master.brand_id = c_product.c_prd_brand_id
                    LEFT JOIN (SELECT t1.* FROM c_pricing t1 WHERE t1.c_price_id  = (SELECT t2.c_price_id  FROM c_pricing t2  WHERE 
                                t2.c_price_prd_id = t1.c_price_prd_id and
                                t2.c_price_effectivedate <= curdate()  and t2.c_price_status="Activated" order by t2.c_price_effectivedate desc, t2.c_price_id desc limit 1  ) ) price_max on price_max.c_price_prd_id = c_product.c_prd_id
                    LEFT JOIN (SELECT t1.* FROM c_product_image t1 WHERE t1.c_prd_img_id  = (SELECT t2.c_prd_img_id  FROM c_product_image t2  WHERE  t2.c_prd_img_prd_id = t1.c_prd_img_prd_id and c_prd_img_status="Activated" order by c_prd_img_id asc limit 1  ) )
                    price_max1 on price_max1.c_prd_img_prd_id = c_product.c_prd_id
                    LEFT JOIN c_color on c_color.c_color_id = c_product.c_prd_color_id
                    LEFT JOIN (select * from c_offer WHERE (curdate() BETWEEN c_off_validity_start_date and c_off_validity_end_date) AND c_off_cmp_id = 10 and c_off_status="Activated") as offer on offer.c_off_prd_id = c_product.c_prd_id';
                    
                    $sql_product_left = DB::select($product_left);
                    // dd($sql_product_left);

                    // $sql_product_left = $sql_product = DB::table('c_product')->where('c_product.c_prd_status', '=', "Activated",'and','c_prd_special_offer','=',"on")->get();
                    // dd($sql_product_left);
                    
                    // while ($productdata_left = $result_product_left->fetch_assoc()) {
                        foreach($sql_product_left as $productdata_left)
                        {
                            $productdataarray_left[] = $productdata_left;
                            $categoryname = $productdata_left->cat_name;
                            if (!in_array($productdata_left->c_pt_id, $productTypearray)) 
                            {
                                array_push($productTypearray, $productdata_left->c_pt_id);
                                $productTypeDataArray[$productdata_left->c_pt_id] = $productdata_left->c_pt_name;
                            }
                        }
                    $total_product = count($sql_product_left);
                    // dd($total_product);

                    $result_home_banner = array();
                    $sql_home_banner = DB::select('select c_bnr_id,c_bnr_name,c_bnr_type_id,c_bnr_img,c_bnr_productType_id,c_bnr_category_id,c_bnr_brand_id,c_bnr_priority,c_bnr_status,c_bnr_path from c_banner where c_bnr_type_id IN (2,9) and c_bnr_status="Activated" order by c_bnr_id desc');
                    $result_home_banner = $sql_home_banner;
                    // dd($result_home_banner);

        return view('pages.special-offer',compact('category','header_product_cats','cats','sql_product','sql_product_left','result_home_banner','result_product'));
    }
}
