<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\C_category;
use App\Models\Product;
use App\Models\C_banner;
use App\Models\C_product_image;
class ProductController extends Controller
{
    //
    public function product_details($id)
    {   
        // $data = Product::find();
        // dd($data);
        $cats = C_category::all();   
        $category = DB::table('c_category')
        ->where('c_category.cat_status','=',"Activated")
        ->where('c_category.cat_key','=',0)
        ->orderBy('c_category.cat_priority','ASC')
        ->get();
        // dd($category);exit;

         $header_product_cats = DB::table('c_product_type')->select('c_pt_name','c_pt_id','c_ptype_img_name','c_ptype_img_path','cat_id')
         ->join('c_category','c_product_type.c_pt_cat_id','=','cat_id')
         ->where('c_product_type.c_pt_status','=',"Activated")
         ->where('c_product_type.c_pt_status','=',"Activated",'and','c_category.cat_id','=','c_product_type.c_pt_cat_id')
        // ->where('c_category.cat_id','=','c_product_type.c_pt_cat_id')
         ->orderBy('c_product_type.c_pt_priority','ASC')
         ->get();
        // dd($header_product_cats);
        // $product = Product::find($id);
         $product = Product::with('cat')->find($id);
        // dd($product);
        // $c_product_img = C_product_image::find($id);
        // dd($c_product_img);

        // dd($product);
        $hone_banner = C_banner::find($id);
        // dd($hone_banner);    
        $sql_splcat = DB::select('SELECT * FROM `c_product_type_specific_category` 
        join c_product_type on c_product_type.c_pt_id = c_product_type_specific_category.c_spf_product_type_id
        JOIN c_product on c_product.c_prd_product_type_id = c_product_type.c_pt_id
        WHERE c_product.c_prd_id = ' . $product['c_prd_id'] . ' and c_product_type_specific_category.c_spf_status="Activated" order by c_product_type_specific_category.c_spf_priority asc');
        // dd($sql_splcat);
        
        $sql_product_img = DB::select('SELECT * FROM c_product_image LEFT JOIN c_product ON c_product.c_prd_id = c_product_image.c_prd_img_prd_id WHERE c_prd_img_name !="" AND c_prd_img_status="Activated"  order by c_prd_img_priority asc');
        // dd($sql_product_img);

        $product_image = DB::table('c_product')
                                ->join('c_product_image','c_product_image.c_prd_img_prd_id','=','c_product.c_prd_id')
                                ->where('c_prd_img_status','=',"Activated",'and','c_prd_img_prd_id','=',$id)
                                ->orderBy('c_prd_img_priority','ASC')
                                ->first();
        // dd($product_image);
        // $product_type = DB::table('')->orderBy()->get();

        

        return view('pages.product_details',compact('category','header_product_cats','product','cats','sql_splcat','sql_product_img','product_image'));
        
    }

}