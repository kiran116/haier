<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class C_product_type extends Model
{
    use HasFactory;
    protected $table = 'c_product_type';
    protected $primaryKey = 'c_pt_id';
    protected $fillable = ['c_pt_name','c_ptype_img_name','c_ptype_img_path'];
    
    public function product_type()
    {
        return $this->belongsTo(C_category::class,'cat_id');
    }
}