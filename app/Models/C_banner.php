<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class C_banner extends Model
{
    use HasFactory;
    protected $table = 'c_banner';
    protected $primaryKey = 'c_bnr_id';
    protected $fillable = ['c_bnr_name','c_bnr_img','c_bnr_path','c_bnr_redirect_link','c_bnr_type_id'];
    
    
}
