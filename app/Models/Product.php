<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'c_product';
    protected $primaryKey = 'c_prd_id';
    protected $fillable = [
        'c_prd_product_type_id','c_prd_brand_id','c_prd_model','c_prd_sku','c_prd_type','c_prd_itemcode','c_prd_itemdesc',
        'c_prd_segcode','c_prd_rating','c_prd_unitprice','c_prd_taxrate','c_prd_status','c_prd_enteredby','c_prd_updatedby',
        'c_prd_name','c_prd_display_name','c_prd_emi','c_new_prd_priority','c_best_sell_product','c_bestsell_prd_priority',
        'c_popular_prod','c_popular_prd_priority','c_prd_exchangeoffer','c_prd_color_id','c_prd_video_path','c_product_priority',
        'c_prd_keywords_seo','c_prd_description_seo','c_product_offer','c_prd_pagetitle','c_prd_footer_text','c_product_image_detail'
    ];
    
    public function catrgory()
    {
        return $this->hasOne(C_category::class);
    }
    
    public function product_type()
    {
        return $this->hasMany(C_product_type::class,'c_pt_cat_id','c_prd_id');
    }
    
}