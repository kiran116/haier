<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class C_category extends Model
{
    use HasFactory;
    protected $table = 'c_category';
    protected $Primary_Key = 'cat_id';
    protected $fillable = ['cat_name','cat_key','cat_priority','cat_iconfile1','cat_filename','cat_status','cat_enteredby','cat_updatedby','cat_timestamp','cat_img_path','cat_icon_path','cat_pagetitle_text','cat_footer_text','cat_discription'];

    public function product()
    {
        return $this->belongsTo(C_product::class,'c_pt_cat_id');
    }

}