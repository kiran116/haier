<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class C_product_image extends Model
{
    use HasFactory;
    protected $table = 'c_product_image';
    protected $primaryKey = 'c_prd_img_id';
    protected $fillable = ['c_prd_img_name','c_prd_img_status'];
}
